<?php
define('MAGENTO_ROOT', getcwd());
define('BASE_DIR', 'catalog/product');
set_time_limit(0);

//set error reporting to see if something is wrong.
error_reporting(E_ALL);
ini_set('display_errors', 1);

//load Magento

$mageFilename = MAGENTO_ROOT .'/app/Mage.php';
if (!file_exists($mageFilename)) {
    echo $mageFilename." was not found";
    exit;
}
require_once $mageFilename;

//set developer mode for additional debug functionality
Mage::setIsDeveloperMode(true);
//instantiate Mage_Core_Model_App;
Mage::app();

//path to base dir catalog/product
$catalogImages = Mage::getBaseDir('media') . DS . BASE_DIR ;


$conn = Mage::getSingleton('core/resource')->getConnection('core_read');

$sql = $conn->select()->from('catalog_product_entity_media_gallery', array('*'));

$galleryImgs = $conn->fetchAll($sql);

//get mediaApi model to manage product's images
$mediaAPI = Mage::getModel('catalog/product_attribute_media_api');
Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);

$countImages = 0;
$countRemoved = 0;
foreach ($galleryImgs as $value){
    $path = $catalogImages . $value['value'];
    if (file_exists($path)) {
        if(!filesize($path) || !is_array(getimagesize($path))){
            $countImages++;
            //remove wrong image from db
            $productID = $value['entity_id'];
            $product = Mage::getModel('catalog/product')->load($productID);
            $items = $mediaAPI->items($product->getId());
            if (count($items)>0) {
                foreach ($items as $item) {
                    if ($item['file'] == $value['value'] ) {
                        $mediaAPI->remove($product->getId(), $item['file']);
                        $countRemoved++;
                    }
                }
            }
        }
    }
}
echo "Found ". $countImages . " wrong image(s).";
echo "<br>";
echo "Removed ". $countRemoved . " wrong image(s).";

