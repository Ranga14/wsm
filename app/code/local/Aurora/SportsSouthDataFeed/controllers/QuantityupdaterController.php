<?php

class Aurora_SportsSouthDataFeed_QuantityupdaterController extends Mage_Core_Controller_Front_Action
{
    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $brandDsUrl = '';
    private $onHandDsUrl = '';
    private $onHandCSVUrl = '';
    private $categoryDsUrl = '';
    private $source = '';
    private $lastUpdate = '';
    private $lastItem = 0;
    private $parentId = 2;
    private $tempDir;
    private $lastQuantityItem;
    private $lastQuantityDate;

    public function indexAction()
    {
        set_time_limit(0);
        $this->getSportsSouthSettings();
        $this->processItems();
        $this->updateLastUpdate();
        $compare = 'https://www.wsmtactical.com/inventory/feedcompare/';
        $temp = file_get_contents($compare);
    }

    public function zeroSportSouthItems()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
        UPDATE aurora_items_ss 
        SET quantity = 0;             
       
        ';
        $writeConnection->query($query);
    }

    public function completeinventoryAction()
    {
        set_time_limit(0);
        $this->zeroSportSouthItems();
        $this->getSportsSouthSettings();

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'quantityFiles/' . date('n/j/Y') . '/';
        $fullFilePath = $folderPath . 'onhandCSV.csv';

        if(!file_exists($fullFilePath))
        {
            if(!is_dir($folderPath))
            {
                mkdir($folderPath,0777, true);
            }

            echo 'download file';

            //We just need the csv data in the middle. No need to turn into xml object
            $csvXml = $this->downloadCompleteInventory();
            $ltrimString = '<?xml version="1.0" encoding="utf-8"?>
<string xmlns="http://webservices.theshootingwarehouse.com/smart/Inventory.asmx">';

            $rtrimString = '
</string>';

            $csvInfo = rtrim(ltrim($csvXml,$ltrimString),$rtrimString);
            $fp = fopen($fullFilePath, 'w');
            fwrite($fp, $csvInfo);
            fclose($fp);
            unset($csvInfo);
            unset($csvXml);
        }

        $csvHandel = fopen($fullFilePath, "r");
        while (($data = fgetcsv($csvHandel, 1000, ",")) !== FALSE)
        {
            try {
                $sportsSouthId = $data[0];
                $quantity = $data[1];
                $price = $data[2];

                if($quantity < 1 || !$sportsSouthId)
                {
                    continue;
                }

                $query = '
            UPDATE aurora_items_ss 
            SET price = :price,
            quantity = :quantity
            WHERE sports_south_id = :sports_south_id ;
            ';

                $bindArray = array(
                    'price' => $price,
                    'quantity' => $quantity,
                    'sports_south_id' => $sportsSouthId
                );
                $writeConnection->query($query, $bindArray);
            }
            catch (Exception $e)
            {
                echo "\n<br/> Error occured - " . $e->getMessage();
                error_log($e);
            }
        }

    }

    private function downloadCompleteInventory()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );
        return $this->getCurlRequest($this->onHandCSVUrl,$data);
    }

    private function updateLastUpdate()
    {

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $query = '
                UPDATE aurora_sports_south
                SET value = \''. date('Y-m-d', time()-86400) . '\'
                WHERE name = \'last_quantity_date\'
            ';
        $writeConnection->query($query);
    }

    private function processItems($brandArray)
    {

        $itemXml = $this->getItems();
        $xml = simplexml_load_string($itemXml);
        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();
        $count = 0;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        foreach ($data_set as $itemSimpleXml)
        {
            $count++;
            $itemNumber = (int)$itemSimpleXml->I;
            $quantity = (int)$itemSimpleXml->Q;
            $price = (float)$itemSimpleXml->P;

            if( !$itemNumber)
            {
                continue;
            }

            try
            {
                $query = 'SELECT * FROM aurora_items_ss WHERE sports_south_id = '. $itemNumber;
                $result = $writeConnection->fetchRow($query);
                if(!$result)
                {
                    //Item not in store skip it
                    continue;
                }
                $magentoId = $result['magento_id'];


                $sql = "
                UPDATE aurora_items_ss
                SET
                price = $price,
                quantity = $quantity,
                for_delete = 0
                WHERE
                magento_id = $magentoId
                ";

                $writeConnection->query($sql);

            }
            catch (Exception $e)
            {
                error_log($e);
            }
        }


    }

    private function getItems()
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'SinceDateTime' => $this->lastQuantityDate . 'T01:01:00.00-01:01',
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->onHandDsUrl,$data);
    }

    private function getSportsSouthSettings()
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'brand_ds_url':
                    $this->brandDsUrl = $result['value'];
                    break;
                case 'on_hand_url':
                    $this->onHandDsUrl = $result['value'];
                    break;
                case 'category_ds_url':
                    $this->categoryDsUrl = $result['value'];
                    break;
                case 'last_update':
                    $this->lastUpdate = $result['value'];
                    break;
                case 'last_item':
                    $this->lastItem = $result['value'];
                    break;
                case 'parent_id':
                    $this->parentId = $result['value'];
                    break;
                case 'last_quantity_item':
                    $this->lastQuantityItem = $result['value'];
                    break;
                case 'last_quantity_date':
                    $this->lastQuantityDate = $result['value'];
                    break;
                case 'onHandCSVUrl':
                    $this->onHandCSVUrl = $result['value'];
                default:
                    break;
            }
        }
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        if(!$results)
        {
            error_log('Could not preform action from URL' . $url);
            error_log(curl_error($ch));
            die();
        }
        return $results;
    }

}