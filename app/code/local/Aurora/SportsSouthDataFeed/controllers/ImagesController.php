<?php

require_once('copy_resize_helper.php');

function resizeImage($file, $superCount=0)
{
    list($source_width, $source_height) = getimagesize($file);
    if($source_width < 1001)
    {
        return false;
    }
    copy_resize($file, $file, 1000);
}

class Aurora_SportsSouthDataFeed_ImagesController extends Mage_Core_Controller_Front_Action
{
    private $customerNumber = 0;
    private $userName = 0;
    private $password = 0;
    private $imageDsUrl = '';
    private $source = '';

    public function indexAction()
    {
        set_time_limit(0);
        echo "starting<br/>\n";
        ob_flush();
        $sportsSouthId = $_GET['ssi'];
        $magentoId = $_GET['mi'];
        $this->getSportsSouthSettings();
        echo "Download images<br/>\n";
        ob_flush();
        $this->processImages($sportsSouthId, $magentoId);
    }

    public function processImages($sportsSouthId,$magentoId)
    {
        $imagesXml = $this->getImages($sportsSouthId,$magentoId);

        $product = Mage::getModel('catalog/product')->load($magentoId);

        $xml = simplexml_load_string($imagesXml);
        $data_set = $xml->children('urn:schemas-microsoft-com:xml-diffgram-v1')->children()->NewDataSet->children();

        $imageCount = 1;
        foreach($data_set as $imageSimpleXml)
        {
            $imageSize = trim($imageSimpleXml->ImageSize);
            $imageLink = trim($imageSimpleXml->Link);
            echo "Loading image{$imageSimpleXml->Link}<br/>\n";
            ob_flush();
            $rawImage = $this->getCurlRequest($imageLink);
            $tempDir = Mage::getBaseDir() . '/tmp/';

            //Get File Name
            $temp = explode('/', $imageLink);
            $fileName = $temp[count($temp) -1];
            $fullFilePath = $tempDir . 'image_' . $imageCount .  $fileName;
            fwrite(fopen($fullFilePath, 'w'), $rawImage);

            if(is_array(getimagesize($fullFilePath))){
                resizeImage($fullFilePath);
                switch($imageSize)
                {
                    case 'large':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                        break;
                    case 'small':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                        break;
                    case 'hires':
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                        break;
                    default:
                        break;
                }
            }

        }
        $product->save();
    }


    private function getImages($sportsSouthId,$magentoId)
    {
        $data =  array(
            'CustomerNumber' => $this->customerNumber,
            'Password' => $this->password,
            'UserName'=> $this->userName,
            'ItemNumber'=> $sportsSouthId,
            'Source'=> $this->source
        );

        return $this->getCurlRequest($this->imageDsUrl,$data);
    }

    private function getSportsSouthSettings()
    {
        $resource = Mage::getSingleton('core/resource');

        $readConnection = $resource->getConnection('core_read');

        $query = 'SELECT name, value FROM aurora_sports_south ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'user_name':
                    $this->userName = $result['value'];
                    break;
                case 'customer_number':
                    $this->customerNumber = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'image_ds_url':
                    $this->imageDsUrl = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }


    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //format data for Windows server service.
        $data_array_string = '';
        foreach($data as $key=>$value)
        {
            $data_array_string .= $key.'='.$value.'&';
        }
        $data_array_string = rtrim($data_array_string,'&');
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_POST,count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_array_string);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }



}

?>