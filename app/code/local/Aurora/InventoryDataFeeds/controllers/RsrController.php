<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 1/22/2016
 * Time: 10:01 PM
 */

require_once('copy_resize_helper.php');

function resizeImage($file, $superCount)
{
    list($source_width, $source_height) = getimagesize($file);
    if($source_width < 1001)
    {
        return false;
    }
    copy_resize($file, $file, 1000);
}


class Aurora_InventoryDataFeeds_RsrController extends Mage_Core_Controller_Front_Action
{
    //Configurable settings
    private $ftp;
    private $userId;
    private $password;
    private $ftpDirectory;
    private $fileName;
    private $ftpImageDirectory;
    private $tempDir;
	private $attributeFileName;
	private $attributesFeedLocation;
    private $lastAttributeRow;
	
	private $wsmAttributes;

    public function indexAction()
    {
        echo '<pre>';
        set_time_limit(0);
        $this->tempDir = Mage::getBaseDir() . '/tmp/';
        $this->getRsrSettings();
        $brands = $this->getBrandArray();
        $categories = $this->getExistingCatagories();
        $this->processFeed($brands, $categories);
        echo '</pre>';
    }
	
	public function attributesAction()
	{
		set_time_limit(0);
		$this->tempDir = Mage::getBaseDir() . '/tmp/';
        $this->getRsrSettings();
		
		$this->wsmAttributes = Mage::getModel('inventory/importerattributes');
		$this->wsmAttributes->setAttributeSetName('WSM RSR');
		
        $downloadedFile = $this->downloadAttributesFeed();
		$this->setAttributesFromFile($downloadedFile);
	}
	
	private function downloadAttributesFeed()
	{
 
 		//Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'attributeFeed/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->attributeFileName;
        if(!file_exists($fullFilePath))
        {
            echo "download file <br/>\n";
            //File isn't here download it
            $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
            $loginAttempt = ftp_login($connection, $this->userId, $this->password);
            if(!$loginAttempt)
            {
                //Couldn't Login
                die();
            }
            $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->attributeFileName , FTP_ASCII);
            if(!$saved)
            {
                echo ('couldn\'t download ' . $this->fileName);
                //Error downloading file
                unlink($fullFilePath);
                die();
            }
            ftp_close($connection);
        }

        return $fullFilePath;		
	}

	private function setAttributesFromFile($downloadedFile)
	{
		$resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $handle = fopen($downloadedFile, 'r');      

        $counter = 0;
        $this->lastAttributeRow --;
        
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
        {
            $counter++;
            if($counter < $this->lastAttributeRow)
            {
                continue;
            }

            try {
                $rsrId = $data[0];
                $query = '
			select magento_id
			from aurora_items_rsr
			where rsr_id = :rsrId';

                $entityId = $writeConnection->fetchOne($query, array('rsrId' => $rsrId));

                if (!$entityId) {
                    continue;
                }

                $query = '
                        SELECT 1 FROM `catalog_product_entity` 
						WHERE  `entity_id`= :magentoId;
                        ';
                $bindArray = array('magentoId' => $entityId);
                $result = $writeConnection->fetchOne($query, $bindArray);

                if (!$result) {
                    continue;
                }

                $this->wsmAttributes->setAttributeField($entityId, 'accessories', $data[2]);
                $this->wsmAttributes->setAttributeField($entityId, 'action', $data[3]);
                $this->wsmAttributes->setAttributeField($entityId, 'type of barrel', $data[4]);
                $this->wsmAttributes->setAttributeField($entityId, 'barrel length', $data[5]);
                $this->wsmAttributes->setAttributeField($entityId, 'catalog code', $data[6]);
                $this->wsmAttributes->setAttributeField($entityId, 'chamber', $data[7]);
                $this->wsmAttributes->setAttributeField($entityId, 'chokes', $data[8]);
                $this->wsmAttributes->setAttributeField($entityId, 'condition', $data[9]);
                $this->wsmAttributes->setAttributeField($entityId, 'capacity', $data[10]);
                $this->wsmAttributes->setAttributeField($entityId, 'description', $data[11]);
                $this->wsmAttributes->setAttributeField($entityId, 'dram', $data[12]);
                $this->wsmAttributes->setAttributeField($entityId, 'edge', $data[13]);
                $this->wsmAttributes->setAttributeField($entityId, 'firing casing', $data[14]);
                $this->wsmAttributes->setAttributeField($entityId, 'finish color', $data[15]);
                $this->wsmAttributes->setAttributeField($entityId, 'fit', $data[16]);
                $this->wsmAttributes->setAttributeField($entityId, 'feet per second', $data[18]);
                $this->wsmAttributes->setAttributeField($entityId, 'frame', $data[19]);
                $this->wsmAttributes->setAttributeField($entityId, 'caliber', $data[20]);
                $this->wsmAttributes->setAttributeField($entityId, 'grain weight', $data[22]);
                $this->wsmAttributes->setAttributeField($entityId, 'grips', $data[23]);
                $this->wsmAttributes->setAttributeField($entityId, 'hand', $data[24]);
                $this->wsmAttributes->setAttributeField($entityId, 'objective', $data[33]);
                $this->wsmAttributes->setAttributeField($entityId, 'ounce of shot', $data[34]);
                $this->wsmAttributes->setAttributeField($entityId, 'packaging', $data[35]);
                $this->wsmAttributes->setAttributeField($entityId, 'power', $data[36]);
                $this->wsmAttributes->setAttributeField($entityId, 'recticle', $data[37]);
                $this->wsmAttributes->setAttributeField($entityId, 'safety', $data[38]);
                $this->wsmAttributes->setAttributeField($entityId, 'sights', $data[39]);
                $this->wsmAttributes->setAttributeField($entityId, 'size', $data[40]);
                $this->wsmAttributes->setAttributeField($entityId, 'type', $data[41]);
                $this->wsmAttributes->setAttributeField($entityId, 'units per box', $data[42]);
                $this->wsmAttributes->setAttributeField($entityId, 'units per case', $data[43]);

                $query = '
                UPDATE aurora_rsr
                SET value = \''. $counter. '\'
                WHERE name = \'lastAttributeRow\'
            ';
                $writeConnection->query($query);
            }
            catch(Exception $e)
            {
                error_log($e);
            }
		}
        $query = '
                UPDATE aurora_rsr
                SET value = \'0\'
                WHERE name = \'lastAttributeRow\'
            ';
        $writeConnection->query($query);
	}

    //Get feed settings from database
    private function getRsrSettings()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = 'SELECT name, value FROM aurora_rsr ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'ftp':
                    $this->ftp = $result['value'];
                    break;
                case 'userId':
                    $this->userId = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'ftpDirectory':
                    $this->ftpDirectory = $result['value'];
                    break;
                case 'fileName':
                    $this->fileName = $result['value'];
                    break;
                case 'ftpImageDirectory':
                    $this->ftpImageDirectory = $result['value'];
					break;
				case 'attributeFileName':
					$this->attributeFileName = $result['value'];
					break;
                case 'lastAttributeRow':
                    $this->lastAttributeRow = $result['value'];
                default:
                    break;
            }
        }
    }

    private function getExistingCatagories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }

    private function getBrandArray()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name, id_primary, brand_id, brand_url, brand_item_count, source, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $writeConnection->fetchAssoc($query);

        return $existingBrandResults;
    }


    private function processFeed($brands, $categories)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $file = $this->downloadFeed();
        $handle = fopen($file, 'r');

        //Get wsm_brand attribute id. Needed for proper sorting
        $query = "
        select attribute_id from eav_attribute e
        where e.attribute_code = 'wsm_brand'
        ";

        $wsmBrandAttributeId = $writeConnection->fetchOne($query);

        $counterSql = 'SELECT MAX(brand_id) FROM aurora_brands';
        $brandCounter = (int) $writeConnection->fetchOne($counterSql);
        
        while (($row = fgetcsv($handle, 1000, ";")) !== FALSE)
        {

            $type = 'simple';
            $product_attribute_set_id = '4';
            $rsrNumber = $row[0];
            $sku = $row[1];
            $itemName = $row[2];
            $departmentId = (int) $row[3];
            $manufacturerId = $row[4];
            $msrp = $row[5];
            $price = $row[6];
            $weight = $row[7];
            $quantity = $row[8];
            $model = $row[9];
            $brand = $row[10];
            $description = $row[13];
			$mapp = 0; //$row[70];

            //Test for UPC. If no UPC use item number as sku
            if(!$row[1])
            {
                $sku = 'rsr' . $rsrNumber;
            }
            elseif(substr($sku, 0, 1) == '0')
            {
                //Get rid of leading zeros to match up with sports south.
                $sku = ltrim($sku, '0');
            }

            //Does Brand already exist? If not insert it
            if(!$brands[$brand])
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, :source)
                ';

                $bindArr = array(
                    'brandName'      => $brand,
                    'brandNo'        => ++$brandCounter,
                    'brandUrl'       => '',
                    'brandItemCount' => 1,
                    'source'         => 'RSR'
                );
                $writeConnection->query($query, $bindArr);

                $brands[$brand] = array(
                    'brand_name'       => $brand,
                    'id_primary'         => $writeConnection->lastInsertId(),
                    'brand_url'        => '',
                    'brand_item_count' => 1,
                    'source'           => 'RSR',
                    'use_mapp'         => 0,
                    'brand_id'         => $brandCounter
                );
            }


            $productCategoryArray = $this->resolveCategory($departmentId, $categories);
            $isInStock = $quantity > 0 ? 1 : 0;

            $productData = array(
                'categories' => $productCategoryArray,
                'name' => $itemName,
                'description' => $itemName,
                'short_description' => $description,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => $weight,
                'price' => (( ((int) $brands[$brand]['use_mapp']) == 1) && $mapp ? $mapp : $price * 1.15),
                'qty' => $quantity,
                'is_in_stock' => $isInStock,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => $msrp,
                'meta_title' => $itemName,
                'meta_description' => $itemName,
                'meta_keyword' => $itemName . ' ' . $brand . ' rsr-' . $rsrNumber,
                'wsm_brand'=> $brands[$brand]['brand_id']
            );

            try
            {

                $query = "select entity_id
                from catalog_product_entity
                where sku = :sku";

                $findProduct = $writeConnection->fetchOne($query, array('sku'=>$sku));

                if(!$findProduct)
                {
                    $newProduct = Mage::getModel('catalog/product_api')->create($type, $product_attribute_set_id, $sku, $productData);
                    $query = '
                                INSERT into aurora_items_rsr
                                (magento_id, upc, rsr_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :rsrId, :price, :msrp, :mapp, :quantity, :brandId )
                                ';

                    $bindArray = array(
                        'magentoId'=> $newProduct,
                        'sku' => $sku,
                        'rsrId' => $rsrNumber ,
                        'price' => $price,
                        'msrp' => $msrp,
						'mapp' => $mapp,
                        'quantity' => $quantity,
                        'brandId' => $brands[$brand]['id_primary']
                    );

                    $writeConnection->query($query, $bindArray);
                    $fullFilePath = $this->getImage($rsrNumber);

                    if($fullFilePath)
                    {
                        $product = Mage::getModel('catalog/product')->load($newProduct);
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                        $product->save();
                    }
                }
                else
                {
                    //Add Brand attribute
                    $query = '
                        REPLACE INTO catalog_product_entity_int
                        (entity_type_id, attribute_id, store_id, entity_id, `value`)
                        VALUES( 4, :wsmBrandAttributeId, 0, :magentoId, :brandNo);
                          ';

                    $bindArray = array (
                        'wsmBrandAttributeId' => $wsmBrandAttributeId,
                        'magentoId' => $findProduct,
                        'brandNo'=>$brands[$brand]['brand_id']
                    );
                    

                    $writeConnection->query($query, $bindArray);

                    $query = "
                    SELECT 1 FROM aurora_items_rsr
                    WHERE upc = :sku";

                    $result = $writeConnection->fetchOne($query,array('sku'=>$sku));

                    if($result)
                    {
                        $query = "
                                UPDATE aurora_items_rsr
                                SET
                                price = :price,
                                msrp = :msrp,
								mapp= :mapp,
                                quantity =  :quantity,
                                aurora_brand_id = :brandId,
                                rsr_id = :rsrId
                                WHERE
                                upc = :sku
                                ";
                        $bindArray = array(
                            'price' => $price,
                            'msrp' => $msrp,
							'mapp' => $mapp,
                            'quantity' => $quantity,
                            'brandId' => $brands[$brand]['id_primary'],
                            'rsrId' => $rsrNumber,
                            'sku' => $sku
                        );
                        $writeConnection->query($query, $bindArray);
                    }
                    else
                    {
                        $query = '
                                INSERT into aurora_items_rsr
                                (magento_id, upc, rsr_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :rsrId, :price, :msrp, :mapp, :quantity, :brandId )
                                ';
                        $bindArray = array(
                            'magentoId'=> $findProduct,
                            'sku' => $sku,
                            'rsrId' => $rsrNumber ,
                            'price' => $price,
                            'msrp' => $msrp,
							'mapp' => $mapp,
                            'quantity' => $quantity,
                            'brandId' => $brands[$brand]['id_primary']
                        );
                        $writeConnection->query($query, $bindArray);
                    }

                }
            }
            catch (Exception $e)
            {
                error_log($e);
            }

        }
    }

    private function getImage($rsrNumber)
    {
        $firstChar = substr($rsrNumber,0,1);
        if(is_numeric($firstChar))
        {
            $firstChar = '#';
        }


        //Download image file to tmp directory
        $folderPath =  $this->tempDir . 'rsrImages/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $rsrNumber . '.jpg';

        echo "download file <br/>\n";
        $connection = ftp_connect($this->ftp);
        if($connection === false)
        {
            return false;
        }
        $loginAttempt = ftp_login($connection, $this->userId, $this->password);
        if(!$loginAttempt)
        {
            //Couldn't Login
            return false;
        }

        $saved = ftp_get($connection, $fullFilePath, $this->ftpImageDirectory . $firstChar . '/' . $rsrNumber . '_1.jpg' , FTP_BINARY);
        if(!$saved)
        {
            echo ('couldn\'t download ' . $rsrNumber . '.jpg');
            //Error downloading file
            unlink($fullFilePath);
            return false;
        }
        ftp_close($connection);

        return $fullFilePath;
    }

    private function resolveCategory($departmentId, $categoryArray)
    {
        $productCategoryArray = array();
        $rsrCategoryMapArray = array(
            0 => 'Other',
            1 => 'Firearms',
            2 => 'Used Guns',
            3 => 'Used Guns',
            4 => 'Other',
            5 => 'Firearms',
            6 => 'NFA | Class 3',
            7 => 'Black Powder Firearms',
            8 => 'Optics',
            9 => 'Optics',
            10 => 'Firearm Accessories',
            11 => 'Firearm Accessories',
            12 => 'Firearm Accessories',
            13 => 'Other',
            14 => 'Firearm Accessories',
            15 => 'Reloading Components',
            16 => 'Black Powder Firearms',
            17 => 'Other',
            18 => 'Ammunition',
            19 => 'Other',
            20 => 'Other',
            21 => 'Other',
            22 => 'Other',
            23 => 'Other',
            24 => 'Firearm Accessories',
            25 => 'Other',
            26 => 'Other',
            27 => 'Other',
            28 => 'Optics',
            29 => 'Optics',
            30 => 'Optics',
            31 => 'Optics',
            32 => 'Firearm Components',
            33 => 'Other',
            34 => 'Firearm Components',
            35 => 'Firearm Accessories',
            36 => 'Other',
            37 => 'Other',
            38 => 'Other',
            39 => 'Other',
            40 => 'Firearm Accessories',
            41 => 'Firearm Components',
            42 => 'Firearm Components',
            43 => 'Firearm Components',
        );

        $newCatString = $rsrCategoryMapArray[$departmentId];




        $productCategoryArray['0'] = (int)( $categoryArray[$newCatString]? : 104 );

        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        if(in_array($newCatString, $firearmArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 97;
        }
        elseif(in_array($newCatString, $opticsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 98;
        }
        elseif(in_array($newCatString, $ammunitionArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 6;
        }
        elseif(in_array($newCatString, $accessoriesArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 100;
        }
        elseif(in_array($newCatString, $componentsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 101;
        }
        elseif(in_array($newCatString, $nfa))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 103;
        }
        elseif(in_array($newCatString, $reloadingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 99;
        }
        elseif(in_array($newCatString, $huntingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 102;
        }
        elseif(in_array($newCatString, $blackPowderArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 19;
        }

        return $productCategoryArray;
    }

    private function downloadFeed()
    {
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'brandXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->fileName;
        if(!file_exists($fullFilePath))
        {
            echo "download file <br/>\n";
            //File isn't here download it
            $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
            $loginAttempt = ftp_login($connection, $this->userId, $this->password);
            if(!$loginAttempt)
            {
                //Couldn't Login
                die();
            }
            $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->fileName, FTP_ASCII);
            if(!$saved)
            {
                echo ('couldn\'t download ' . $this->fileName);
                //Error downloading file
                unlink($fullFilePath);
                die();
            }
            ftp_close($connection);
        }

        return $fullFilePath;
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPGET,1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }
}

