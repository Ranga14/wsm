<?php

class Aurora_InventoryDataFeeds_LipseysimporterController extends Mage_Core_Controller_Front_Action
{

    private $importer;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $this->importer = Mage::getModel('inventory/lipseysimporter');
        parent::__construct($request, $response, $invokeArgs);
    }

    public function indexAction()
    {
        echo '<pre>';
        $this->importer->processInventoryFeed();
        echo '</pre>';
    }

    public function quantityAction()
    {
        echo '<pre>';
        $this->importer->processQuantityFeed();
        echo '</pre>';
    }
}