<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 1/22/2016
 * Time: 10:01 PM
 */

require_once('copy_resize_helper.php');

function resizeImage($file, $superCount)
{
    list($source_width, $source_height) = getimagesize($file);
    if($source_width < 1001)
    {
        return false;
    }
    copy_resize($file, $file, 1000);
}


class Aurora_InventoryDataFeeds_ChattanoogaController extends Mage_Core_Controller_Front_Action
{
    //Configurable settings
    private $ftp;
    private $userId;
    private $password;
    private $ftpDirectory;
    private $fileName;
    private $wsmAttributes;
    private $tempDir;

    public function indexAction()
    {
        set_time_limit(0);

        //Setup Attributes Model
        $this->wsmAttributes = Mage::getModel('inventory/importerattributes');
        $this->wsmAttributes->setAttributeSetName('WSM Davidson');

        //Tmp directory for feeds and images
        $this->tempDir = Mage::getBaseDir() . '/tmp/';

        //Get settings from database
        $this->getChattanoogaSettings();

        //Setup brands and categories
        $brands = $this->getBrandArray();
        $categories = $this->getExistingCatagories();

        //Process Feed
        $this->downloadFeed($brands, $categories);
    }

    //Get feed settings from database
    private function getChattanoogaSettings()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = 'SELECT name, value FROM aurora_davidson ';
        $results = $readConnection->fetchAll($query);

        foreach($results as $result)
        {
            switch($result['name'])
            {
                case 'ftp':
                    $this->ftp = $result['value'];
                    break;
                case 'userId':
                    $this->userId = $result['value'];
                    break;
                case 'password':
                    $this->password = $result['value'];
                    break;
                case 'ftpDirectory':
                    $this->ftpDirectory = $result['value'];
                    break;
                case 'fileName':
                    $this->fileName = $result['value'];
                    break;
                default:
                    break;
            }
        }
    }

    private function downloadFeed($brands, $categories)
    {}

    private function getImage($davidsonItemNo)
    {
        $imageLink = 'http://www.galleryofguns.com/prod_images/' . trim($davidsonItemNo) . '.jpg' ;
        $rawImage = $this->getCurlRequest($imageLink);
        $tempDir = $this->tempDir;
        //Get File Name
        $temp = explode('/', $imageLink);
        $fileName = $temp[count($temp) -1];
        $fullFilePath = $tempDir . 'image_'  .  $fileName;
        fwrite(fopen($fullFilePath, 'w'), $rawImage);
        resizeImage($fullFilePath);
        return $fullFilePath;
    }

    private function resolveCategory($catString, $categoryArray)
    {
        $productCategoryArray = array();
        $newCatString = '';

        switch($catString)
        {
            case 'Pistol: Semi-Auto':
            case 'Pistol: Derringer' :
            case 'Pistol: Lever Action':
            case 'Pistol: Double Action Only':
                $newCatString = 'Pistols';
                break;
            case 'Rifle: Semi-Auto':
            case 'Rifle: Bolt Action':
            case 'Rifle: Single Shot':
            case 'Rifle|Shotgun Combo: All':
            case 'Rifle: Bolt Action':
            case 'Rifle: Lever Action':
            case 'Rifle: Pump Action':
                $newCatString = 'Rifles';
                break;
            case 'Shotgun: Semi-Auto':
            case 'Shotgun: Pump Action':
            case 'Shotgun: Over and Under':
            case 'Shotgun: Lever Action':
            case 'Shotgun: Single Shot':
            case 'Shotgun: Side by Side':
                $newCatString = 'Shotguns';
                break;
            case 'Revolver: Double Action':
            case 'Revolver: Double Action Only':
            case 'Revolver: Single Action':
                $newCatString = 'Revolvers';
                break;
            case 'Pistol: Semi-Auto Air':
            case 'Rifle: Semi-Auto Air':
                $newCatString = 'Air guns';
                break;
            default:
                $newCatString = 'Other';
                break;

        }

        $productCategoryArray['0'] = (int)( $categoryArray[$newCatString]? : 104 );

        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        if(in_array($newCatString, $firearmArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 97;
        }
        elseif(in_array($newCatString, $opticsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 98;
        }
        elseif(in_array($newCatString, $ammunitionArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 6;
        }
        elseif(in_array($newCatString, $accessoriesArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 100;
        }
        elseif(in_array($newCatString, $componentsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 101;
        }
        elseif(in_array($newCatString, $nfa))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 103;
        }
        elseif(in_array($newCatString, $reloadingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 99;
        }
        elseif(in_array($newCatString, $huntingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 102;
        }
        elseif(in_array($newCatString, $blackPowderArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 19;
        }

        return $productCategoryArray;
    }

    private function getExistingCatagories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }

    private function getBrandArray()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = '
          SELECT brand_name, id_primary, brand_id, brand_url, brand_item_count, source, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $writeConnection->fetchAssoc($query);

        return $existingBrandResults;
    }

    private function getCurlRequest($url,$data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPGET,1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $results = curl_exec($ch);
        curl_close($ch);

        return $results;
    }
}