<?php

require_once('Importerbase.php');

class Aurora_InventoryDataFeeds_Model_Lipseysimporter extends ImporterBase
{
    public function _construct()
    {
        parent::_construct();
        $this->settingsTable = 'aurora_lipseys'; //Settings for this feed
        $this->itemsTable = 'aurora_items_lipseys'; //keeping track of our items
        $this->getFeedSettings();
        $this->wsmAttributes->setAttributeSetName('WSM Lipseys');
        $this->brandSource = 'Lipseys';
    }
    
    

    public function processInventoryFeed()
    {
        echo 'startDownload
        ';
        $feedFile = $this->downloadInventoryFeed();
        $xml = simplexml_load_file($feedFile);
        $dataSet = $xml->children();

        //Loop over parsed xml
        foreach($dataSet as $data)
        {
            //Pull data from xml entry
            $itemNo = $data->ItemNo;
            $desc1 = $data->Desc1;
            $desc2 = $data->Desc2;
            $upc = $data->UPC;
            $mfgModelNo = $data->MFGModelNo;
            $msrp = $data->MSRP;
            $model = $data->Model;
            $caliber = $data->Caliber;
            $mfg = trim($data->MFG);
            $finish = $data->Finish;
            $length = $data->Length;
            $receiver = $data->Receiver;
            $safety = $data->Safety;
            $sights = $data->Sights;
            $stockFrameGrips = $data->StockFrameGrips;
            $magazine = $data->Magazine;
            $weight = $data->Weight;
            $image = $data->Image;
            $chamber = $data->Chamber;
            $drilledTapped = $data->DrillTapped;
            $rateOfTwist = $data->RateOfTwist;
            $itemType = $data->ItemType;
            $feature1 = $data->Feature1;
            $feature2 = $data->Feature2;
            $feature3 = $data->Feature3;
            $shippingWeight = $data->ShippingWeight;
            $boundBookModel = $data->BoundBookModel;
            $boundBookType = $data->BoundBookType;
            $boundBookMFG = $data->BoundBookMFG;
            $nfaThreadPattern = $data->NFAThreadPattern;
            $nfaAttachMethod = $data->NFAAttachMethod;
            $nfaBaffle = $data->NFABaffle;
            $nfaCanDisassemble = $data->NFACanDisassemble;
            $nfaConstruction = $data->NFAConstruction;
            $nfadbReduction = $data->NFAdbReduction;
            $nfaDiameter = $data->NFADiameter;
            $nfaForm3Caliber = $data->NFAForm3Caliber;
            $magnification = $data->Magnification;
            $maintube = $data->Maintube;
            $objective = $data->Objective;
            $adjustableObjective = $data->AdjustableObjective;
            $opticAdjustments = $data->OpticAdjustments;
            $reticle = $data->Reticle;
            $illuminatedReticle = $data->IlluminatedReticle;

            //Prepare some data
            $categoryArray = $this->resolveCategory($itemType);
            $this->checkBrandExists($mfg);
            $upc = $upc? $upc : 'lip'. $itemNo;

            //Magento product to insert
            $productData = array(
                'categories' => $categoryArray,
                'name' => $model . ' ' . $itemNo,
                'description' => $desc1 . ' <br/>' . $desc2,
                'short_description' => $desc1 . ' <br/>' . $desc2,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => $weight,
                'price' => 0,
                'qty' => 0.00,
                'is_in_stock' => 0,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => $msrp,
                'meta_title' => $itemNo,
                'meta_description' => $itemNo,
                'meta_keyword' => $mfg . ' lipseys-' . $itemNo,
                'wsm_brand'=> $this->brands[$mfg]['brand_id']
            );

            try
            {
                //Insert product into Magento and aurora tables.
                $insertProduct = $this->insertProduct($productData, $upc, $itemNo, $mfg);
                if($insertProduct['newItem'])
                {
                    $file = $this->downloadImage($image);
                    $product = Mage::getModel('catalog/product')->load($insertProduct['magentoId']);
                    $product->addImageToMediaGallery($file, array('small_image','thumbnail', 'image'), true);
                    $product->save();
                }

                //Insert Attributes
                $entityId = $insertProduct['magentoId'];
                $this->wsmAttributes->setAttributeField($entityId, 'safety', $safety);
                $this->wsmAttributes->setAttributeField($entityId, 'length', $length);
                $this->wsmAttributes->setAttributeField($entityId, 'receiver', $receiver);
                $this->wsmAttributes->setAttributeField($entityId, 'sights', $sights);
                $this->wsmAttributes->setAttributeField($entityId, 'stock frame grips', $stockFrameGrips);
                $this->wsmAttributes->setAttributeField($entityId, 'magazine', $magazine);
                $this->wsmAttributes->setAttributeField($entityId, 'chamber', $chamber);
                $this->wsmAttributes->setAttributeField($entityId, 'drilled tapped', $drilledTapped);
                $this->wsmAttributes->setAttributeField($entityId, 'rate of twist', $rateOfTwist);
                $this->wsmAttributes->setAttributeField($entityId, 'feature1', $feature1);
                $this->wsmAttributes->setAttributeField($entityId, 'feature2', $feature2);
                $this->wsmAttributes->setAttributeField($entityId, 'feature3', $feature3);
                $this->wsmAttributes->setAttributeField($entityId, 'shipping weight', $shippingWeight);
                $this->wsmAttributes->setAttributeField($entityId, 'bound book model', $boundBookModel);
                $this->wsmAttributes->setAttributeField($entityId, 'bound book type', $boundBookType);
                $this->wsmAttributes->setAttributeField($entityId, 'finish color', $finish);
                $this->wsmAttributes->setAttributeField($entityId, 'bound book mfg', $boundBookMFG);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa thread pattern', $nfaThreadPattern);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa attach method', $nfaAttachMethod);
                $this->wsmAttributes->setAttributeField($entityId, 'caliber', $caliber);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa baffle', $nfaBaffle);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa can disassemble', $nfaCanDisassemble);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa construction', $nfaConstruction);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa db reduction', $nfadbReduction);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa diameter', $nfaDiameter);
                $this->wsmAttributes->setAttributeField($entityId, 'nfa form 3 caliber', $nfaForm3Caliber);
                $this->wsmAttributes->setAttributeField($entityId, 'magnification', $magnification);
                $this->wsmAttributes->setAttributeField($entityId, 'maintube', $maintube);
                $this->wsmAttributes->setAttributeField($entityId, 'objective', $objective);
                $this->wsmAttributes->setAttributeField($entityId, 'adjustable objective', $adjustableObjective);
                $this->wsmAttributes->setAttributeField($entityId, 'optic adjustments', $opticAdjustments);
                $this->wsmAttributes->setAttributeField($entityId, 'reticle', $reticle);
                $this->wsmAttributes->setAttributeField($entityId, 'illuminated reticle', $illuminatedReticle);

            }
            catch (Exception $e)
            {
                var_dump($e);
                die();
                error_log($e);
            }
        }
        echo 'Finish Download
        ';
    }
    
    private function downloadInventoryFeed()
    {
        $url = $this->itemsUrl . '?email=' . $this->userName . '&pass=' . $this->password;

        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'FeedXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . 'lipseysInventory.xml';
        if(!file_exists($fullFilePath))
        {
            echo "begin download file <br/>\n";
            file_put_contents($fullFilePath, fopen($url, 'r'));
            echo "finished download file <br/>\n";
        }

        return $fullFilePath;
    }

    private function downloadImage($image)
    {
        $imageLink = 'http://www.lipseys.net/images/' . trim($image) ;
        $rawImage = $this->getCurlRequest($imageLink);
        $fullFilePath = $this->tempDir . $image;
        fwrite(fopen($fullFilePath, 'w'), $rawImage);
        resizeImage($fullFilePath);
        return $fullFilePath;
    }

    public function processQuantityFeed()
    {
        echo 'startDownload
        ';
        $feedFile = $this->downloadQuantityFeed();
        $xml = simplexml_load_file($feedFile);
        $dataSet = $xml->children();

        //Loop over parsed xml
        foreach($dataSet as $data)
        {
            try {
                $query = "
                UPDATE aurora_items_lipseys
                SET
                price = :price,                                
                quantity =  :quantity,
                mapp = :mapp
                WHERE
                upc = :upc
                ";

                $bindArray = array(
                    'price' => $data->Price,
                    'quantity' => $data->QtyOnHand,
                    'mapp' => $data->RetailMAP,
                    'upc' => $data->UPC
                );
                $this->writeConnection->query($query, $bindArray);
            }
            catch(Exception $e)
            {
                var_dump($e);die();
                error_log($e);
            }
        }

        //Update any items that might have a price of 0 in case the feed resolver could miss them later
        $query='
        
          UPDATE catalog_product_entity_decimal cped
          left join aurora_items_lipseys ai
          on  cped.entity_id = ai.magento_id
          left join eav_attribute ea
          on cped.attribute_id = ea.attribute_id
          left join aurora_brands ab
          on ai.aurora_brand_id = ab.id_primary
          SET cped.value = (ai.price * 1.15)
          where ai.id is not null
          and ea.attribute_code = \'price\'          
          and cped.value < 0.1;

        ';
        
        $this->writeConnection->query($query);
    }

    private function downloadQuantityFeed()
    {
        $url = $this->quantityUrl . '?email=' . $this->userName . '&pass=' . $this->password;
        echo "\nURL - ". $url . "\n";
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'QuantityXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . 'lipseysQuantityAndPrice.xml';
        if(!file_exists($fullFilePath))
        {
            echo "\nbegin download file <br/>\n";
            file_put_contents($fullFilePath, fopen($url, 'r'));
            echo "\nfinished download file <br/>\n";
        }

        return $fullFilePath;
    }

}