<?php

class Aurora_InventoryDataFeeds_Model_Importerattributes extends Mage_Core_Model_Abstract
{
	private $writeConnection;
    private $readConnection;
	private $attributeSetName;
	private $attributeSetArray;
	private $attributeFieldIds;
	private $attributeSetId;
	private $entityTypeId;
	private $attributeGroupId;
    private $currentEntityId;
	
	public function _construct()
    {	
        parent::_construct();
        $this->_init('inventory/importerattributes');
        $this->entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId(); //product entity type
        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
		$this->attributeSetArray = array();
        $this->currentEntityId = 0;
    }
	
	public function setAttributeSetName($setName)
	{
		$this->attributeSetId = $this->getAttributeSetId($setName);
		$this->attributeSetName = $setName;
		$this->attributeGroupId = $this->getAttributeGroupId($setName, $this->attributeSetId);
	}
	
	public function setAttributeField($entityId, $field, $value)
	{
		$field = trim($field);
		$value = trim($value);
		
		if(!$field)// || !$value)
		{
			//Can't do anything without field or value
			return false;
		}
		
		//Get FieldId if we don't already have it. Create field if it doesn't exist yet
		if (!array_key_exists($field, $this->attributeSetArray))
		{
			$this->attributeFieldIds[$field] = $this->getFieldId($field);
			$this->attributeSetArray[$field] = array();
		}

		//Get value if we don't already have it. Create value if it doesn't exist yet. Values are represented as int for faster searches
		if (!array_key_exists($value, $this->attributeSetArray[$field]))
		{
			$this->attributeSetArray[$field][$value] = $value ? $this->getFieldValueId($field, $value) : 0;
		}

        if($entityId != $this->currentEntityId)
        {
            //Change attribute set
            $query = '
        UPDATE `catalog_product_entity` SET `attribute_set_id`= :attributeId WHERE `entity_id`= :magentoId;
        ';
            $bindArray = array(
                'magentoId' => $entityId,
                'attributeId' => $this->attributeSetId
            );

            $this->writeConnection->query($query, $bindArray);
            $this->currentEntityId = $entityId;
        }

		$query = '
        SELECT value_id FROM catalog_product_entity_int
        WHERE attribute_id = :attributeId
        AND entity_id = :entityId
        ';

        $bindArray = array (
        	'attributeId' => $this->attributeFieldIds[$field],
            'entityId' => $entityId
        );
        $valueId = $this->writeConnection->fetchOne($query, $bindArray);

        if($valueId)
        {
        	$query = '
            UPDATE catalog_product_entity_int
            SET `value` = :value
            WHERE attribute_id = :attributeId
            AND entity_type_id = :magentoId
            ';

            $bindArray = array(
				'value' => $this->attributeSetArray[$field][$value],
				'attributeId' => $this->attributeFieldIds[$field],
				'magentoId' => $entityId,
            );
                                
			$this->writeConnection->query($query, $bindArray);
		}
        else
        {
            if(!$value)
            {
                $query = '
            DELETE FROM catalog_product_entity_int
            WHERE attribute_id = :attributeId
            AND entity_type_id = :magentoId;
            ';

                $bindArray = array(
                    'attributeId' => $this->attributeFieldIds[$field],
                    'magentoId' => $entityId,
                );

            }
            else {
                $query = '
            INSERT INTO catalog_product_entity_int
            (entity_type_id, attribute_id, store_id, entity_id, `value`)
            VALUES( 4, :attributeId, 0, :magentoId, :value);
            ';

                $bindArray = array(
                    'attributeId' => $this->attributeFieldIds[$field],
                    'magentoId' => $entityId,
                    'value' => $this->attributeSetArray[$field][$value]
                );
            }
			$this->writeConnection->query($query, $bindArray);
		}
	}
	
	private function getFieldId($field)
	{
        $attribute_label = ucwords(strtolower(trim($field)));
        $attribute_code = 'wsm_' . str_replace(' ', '_', strtolower($attribute_label));

        $sql = '
        SELECT 	attribute_id
        FROM eav_attribute
        WHERE attribute_code = :code
        ';
        $bindArray = array('code'=>$attribute_code);
        $id = $this->readConnection->fetchOne($sql, $bindArray);

        $helper = Mage::helper('catalog/product');
        $attributeField = Mage::getModel('catalog/resource_eav_attribute');
		
		// Item doesn't exist create it		
        if (!$id) 
		{
            $frontend_input = 'select'; // dropdown field
            $data = array(
                'attribute_code' => $attribute_code,
                'frontend_label' => array(
                    0 => $attribute_label, // admin label
                    1 => $attribute_label, // user label
                ),
                'frontend_input' => $frontend_input,
                'source_model' => $helper->getAttributeSourceModelByInputType($frontend_input),
                'backend_model' => $helper->getAttributeBackendModelByInputType($frontend_input),
                'is_configurable' => 0,
                'is_filterable' => 0,
                'is_filterable_in_search' => 0,
                'backend_type' => $attributeField->getBackendTypeByInput($frontend_input),
                'default_value' => '',
                'apply_to' => array(),
            );
            $attributeField->addData($data);
        } else {
            $attributeField->load($id);
        }
        
        $attributeField->setEntityTypeId($this->entityTypeId);
        $attributeField->setIsUserDefined(1);
        $attributeField->setAttributeSetId($this->attributeSetId);
        $attributeField->setAttributeGroupId($this->attributeGroupId);
        $attributeField->save();
        $attr_field_id = $attributeField->getId();

        return $attr_field_id;
    }
	
	private function getAttributeSetId($setName)
	{
		$sql = '
        SELECT 	attribute_set_id
        FROM eav_attribute_set
        WHERE attribute_set_name = :name
        ';
        $bindArray = array('name'=>$setName);
        $id = $this->readConnection->fetchOne($sql, $bindArray);

        // Item exists return id
        if($id)
            return $id;

        //Item does not exist so create it.
        $baseAttributeSet = 4; // "default" attribute set
        $attributeSet = Mage::getModel('eav/entity_attribute_set'); //instantiate the model
        $attributeSet->setEntityTypeId($this->entityTypeId);//attribute set is used for products
        $attributeSet->setAttributeSetName(trim($setName));//set the attribute set name
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($baseAttributeSet);
        $attributeSet->save();
        $attr_set_id = $attributeSet->getId();

        return $attr_set_id;
	}
	
	private function getAttributeGroupId($setName, $setId)
	{
		 $sql = '
        SELECT 	attribute_group_id
        FROM eav_attribute_group
        WHERE attribute_group_name = :name
        AND attribute_set_id = :setId
        ';
        $bindArray = array('name'=>$setName, 'setId'=>$setId);
        $id = $this->readConnection->fetchOne($sql, $bindArray);

        // Item exists return id
        if($id)
            return $id;

        $attributeGroup = Mage::getModel('eav/entity_attribute_group');
        $attributeGroup->setAttributeGroupName($setName)
            ->setAttributeSetId($setId);
        $attributeGroup->save();
        $id = $attributeGroup->getId();

        return $id;
	}

	private function getFieldValueId($field, $value)
	{
		$fieldId = $this->attributeFieldIds[$field];
		$attributeField = Mage::getModel('catalog/resource_eav_attribute');
        $attributeField->load($fieldId);
        $attr_option_id = $this->getAttributeOptionIdByName($attributeField, $value);
        if (!$attr_option_id) {
            $data['option'] = array(
                'value' => array('option_0' => array($value, $value)),
                'order' => array('option_0' => 1),
            );
            $attributeField->addData($data);
            $attributeField->save();
            $attr_option_id = $this->getAttributeOptionIdByName($attributeField, $value);
        }
        return $attr_option_id;
	}
	
	private function getAttributeOptionIdByName($attributeField, $option_label)
    {
        $attr_option_id = null;
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute_options_model->setAttribute($attributeField);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $option_label) {
                $attr_option_id = $option['value'];
                break;
            }
        }

        return $attr_option_id;
    }
}