<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 8/27/2015
 * Time: 10:01 PM
 */

class Aurora_TopSelling_NewArrivalsController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $bestSellers = $this->getBestSellersCollection();
        Mage::register('bestSellers', $bestSellers);
        $this->loadLayout();
        $this->renderLayout();
    }

    public function getBestSellersCollection()
    {
        $_productCollectionNew = Mage::getModel('catalog/product')->getCollection();

        $_productCollectionNew
            ->addAttributeToSelect('*');

        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($_productCollectionNew);

        $_productCollectionNew ->addAttributeToFilter('image', array('neq' => 'no_selection'))
            ->setVisibility(array(2, 3, 4))
            ->setOrder('created_at', 'desc')
            ->setPage(1, 16)
        ;



        $imageHelper = Mage::helper('catalog/image');

        $bestSeller = array();

        foreach($_productCollectionNew as $bs)
        {
            $label = $bs->getData('small_image_label');
            if (empty($label)) {
                $label = $bs->getName();
            }
            $entity_id = $bs->getData('entity_id');
            $bestSeller[$entity_id]['entity_id'] = $entity_id;
            $bestSeller[$entity_id]['name'] = $bs->getData('name');
            $bestSeller[$entity_id]['url_path'] = $bs->getProductUrl();
            $bestSeller[$entity_id]['price'] = $bs->getFormatedPrice();
            $bestSeller[$entity_id]['smallImageLabel'] = strip_tags($label);
            $bestSeller[$entity_id]['smallImage'] = (string)$imageHelper->init($bs, 'small_image')->resize(240);
            $bestSeller[$entity_id]['msrp'] = number_format($bs->getMsrp(), 2);
            $bestSeller[$entity_id]['rattingsSummary'] = $bs->getRatingSummary();
            $bestSeller[$entity_id]['reviewsSummary'] = $bs->getReviewsSummaryHtml('short');
            $bestSeller[$entity_id]['saleable'] = $bs->isSaleable();
            $bestSeller[$entity_id]['cartUrl'] = $bs->getAddToCartUrl();
            $bestSeller[$entity_id]['name'] = $bs->getData('name');
            $bestSeller[$entity_id]['shortDescription'] = $bs->getShortDescription();
        }

        return $bestSeller;
    }
}