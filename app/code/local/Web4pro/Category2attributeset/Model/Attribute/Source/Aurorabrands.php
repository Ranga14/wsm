<?php

class Web4pro_Category2attributeset_Model_Attribute_Source_Aurorabrands extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if(!$this->_options){
            $collection = Mage::getModel('web4procategory2attributeset/aurorabrands')->getCollection();
            $collection->addFieldToSelect(array('id_primary', 'brand_id', 'brand_name'));
            $collection->setOrder('brand_name', 'ASC');
            $collection->load();
            $this->_options = array();
            foreach ($collection as $item) {
                $this->_options[] = array(
                    'value' => $item->getBrandId(),
                    'label' => $item->getBrandName(),
                );
            }
        }


        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }

        return $_options;
    }
}

			 