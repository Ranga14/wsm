<?php

class Web4pro_Category2attributeset_Model_Category2attributeset extends Mage_Core_Model_Abstract
{
    private $entityTypeId;
    private $categoryFieldMap;
    private $resource;
    private $writeConnection;
    private $readConnection;

    public function _construct()
    {
        parent::_construct();
        $this->_init('web4procategory2attributeset/category2attributeset');

        $this->entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId(); //product entity type

        $this->resource = Mage::getSingleton('core/resource');
        $this->writeConnection = $this->resource->getConnection('core_write');
        $this->readConnection = $this->resource->getConnection('core_read');
    }

    public function saveCategoriesCreateAttributeSets($categories = array())
    {
        $cnt = count($categories);
        echo "Begin to add {$cnt} attr. sets <br>";

        $itatr2FieldMap = array('set' => array(), 'category' => array());
        $i = 0;
        foreach ($categories as $category) {
            $category_name = 'WSM ' . trim($category['categoryDescription']);

            $attr_set_id = $this->addAttributeSet($category_name);
            $attr_group_id = $this->addAttributeGroup($category_name, $attr_set_id);
            $this->updateCategoryByName($category_name, $attr_set_id);

            $itatr2FieldMap['category'][$category['categoryId']] = array();
            $itatr2FieldMap['set'][$category['categoryId']] = $attr_set_id;

            foreach ($category as $fkey => $fitem) {
                if (preg_match('/^att\d{1,2}$/', $fkey) && !empty($fitem)) {
                    $attr_field_id = $this->addAttributeField($fitem, $attr_set_id, $attr_group_id);

                    // make key like this ITATR2. Key use here Aurora/SportsSouthDataFeed/controllers/IndexController
                    $xml_attr_key = str_replace('att', 'ITATR', $fkey);
                    $itatr2FieldMap['category'][$category['categoryId']][$xml_attr_key] = $attr_field_id;
                }
            }
            $i++;
            echo "Attr. set `{$category_name}`, {$i} of {$cnt} was added <br>";
            flush();
        }
        $this->categoryFieldMap = $itatr2FieldMap;

        echo "Finish add attr. sets <br><br>";
    }

    public function getCategoryFieldMap()
    {
        return $this->categoryFieldMap;
    }

    public function addOptionToAttributeField($xml_cat_id, $xml_data)
    {
        $field_option_map = array();

        if (is_object($xml_data)) {
            $xml_data = get_object_vars($xml_data);
        }
        foreach ($xml_data as $fkey => $label) {
            $label = trim($label);
            $attr_field_id = null;
            if(isset($this->categoryFieldMap['category'][$xml_cat_id][$fkey])) {
                $attr_field_id = $this->categoryFieldMap['category'][$xml_cat_id][$fkey];
            }

            if (!is_object($label) && preg_match('/^ITATR\d{1,2}$/', $fkey) && !empty($label) && $attr_field_id) {
                $attr_option_id = $this->addAttributeFieldOption($attr_field_id, $label);
                $attr_field_code = $this->getAttributeFieldCode($attr_field_id);

                $field_option_map[$attr_field_code] = $attr_option_id;
            }
        }

        return $field_option_map;
    }


    protected function getIdByCategoryName($name)
    {
        $model = Mage::getModel('web4procategory2attributeset/category2attributeset');
        $model->load($name, 'category_name');

        return $model->getId();
    }

    protected function updateCategoryByName($category_name, $attr_set_id)
    {
        $id = false;
        $sql = '
        SELECT *
        FROM web4pro_category2attributeset_entities
        WHERE category_name = :name
        ';
        $bindArray = array('name'=>$category_name);
        $row = $this->readConnection->fetchRow($sql, $bindArray);

        if($row)
        {
            if($row['category_name'] == $category_name && $row['attributeset_id'] == $attr_set_id)
            {
                return $row['c2as_id'];
            }
            $id = $row['id'];
        }

        $model = Mage::getModel('web4procategory2attributeset/category2attributeset');
        $model->setData(array('category_name' => $category_name, 'attributeset_id' => $attr_set_id));
        if ($id) {
            $model->setId($id);
        }
        $model->save();
        $id = $model->getId();

        return $id;
    }

    /**
     * Create attribute Set if it does not exist
     *
     * @param string $attr_set_name Attr. Set name
     *
     * @return int Attribute Set ID
     *
     */
    protected function addAttributeSet($attr_set_name)
    {
        $sql = '
        SELECT 	attribute_set_id
        FROM eav_attribute_set
        WHERE attribute_set_name = :name
        ';
        $bindArray = array('name'=>$attr_set_name);
        $id = $this->readConnection->fetchOne($sql, $bindArray);

        // Item exists return id
        if($id)
            return $id;

        //Item does not exist don't create it.
        $baseAttributeSet = 4; // "default" attribute set
        $attributeSet = Mage::getModel('eav/entity_attribute_set'); //instantiate the model
        $attributeSet->setEntityTypeId($this->entityTypeId);//attribute set is used for products
        $attributeSet->setAttributeSetName(trim($attr_set_name));//set the attribute set name
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($baseAttributeSet);
        $attributeSet->save();
        $attr_set_id = $attributeSet->getId();


        return $attr_set_id;
    }

    /**
     * Create attribute Group if it does not exist
     *
     * @param string $attr_group_name Attr. Group name
     * @param int    $attr_set_id     Attr. Set ID
     *
     * @return int attribute Group ID
     *
     */
    protected function addAttributeGroup($attr_group_name, $attr_set_id)
    {
        $sql = '
        SELECT 	attribute_group_id
        FROM eav_attribute_group
        WHERE attribute_group_name = :name
        AND attribute_set_id = :setId
        ';
        $bindArray = array('name'=>$attr_group_name, 'setId'=>$attr_set_id);
        $id = $this->readConnection->fetchOne($sql, $bindArray);

        // Item exists return id
        if($id)
            return $id;

        $attributeGroup = Mage::getModel('eav/entity_attribute_group');
        $attributeGroup->setAttributeGroupName($attr_group_name)
            ->setAttributeSetId($attr_set_id);
        $attributeGroup->save();
        $attr_group_id = $attributeGroup->getId();

        return $attr_group_id;
    }

    /**
     * Create attribute Field if it does not exist and attach it to a Group
     *
     * @param string $attribute_label Attr. Field label
     * @param int    $attr_set_id     Attr. Set ID
     * @param string $attr_group_id   Attr. Group ID
     *
     * @return int attribute Field ID
     *
     */
    protected function addAttributeField($attribute_label, $attr_set_id, $attr_group_id)
    {
        $attribute_label = ucwords(strtolower(trim($attribute_label)));
        $attribute_code = 'wsm_' . str_replace(' ', '_', strtolower($attribute_label));

        $sql = '
        SELECT 	attribute_id
        FROM eav_attribute
        WHERE attribute_code = :code
        ';
        $bindArray = array('code'=>$attribute_code);
        $attr_field_id = $this->readConnection->fetchOne($sql, $bindArray);

        $helper = Mage::helper('catalog/product');
        $attributeField = Mage::getModel('catalog/resource_eav_attribute');

        if (!$attr_field_id) { // is not exists
            $frontend_input = 'select'; // dropdown field
            $data = array(
                'attribute_code' => $attribute_code,
                'frontend_label' => array(
                    0 => $attribute_label, // admin label
                    1 => $attribute_label, // user label
                ),
                'frontend_input' => $frontend_input,
                'source_model' => $helper->getAttributeSourceModelByInputType($frontend_input),
                'backend_model' => $helper->getAttributeBackendModelByInputType($frontend_input),
                'is_configurable' => 0,
                'is_filterable' => 0,
                'is_filterable_in_search' => 0,
                'backend_type' => $attributeField->getBackendTypeByInput($frontend_input),
                'default_value' => '',
                'apply_to' => array(),
            );
            $attributeField->addData($data);
        } else {
            $attributeField->load($attr_field_id);
        }
        $attributeField->setEntityTypeId($this->entityTypeId);
        $attributeField->setIsUserDefined(1);
        $attributeField->setAttributeSetId($attr_set_id);
        $attributeField->setAttributeGroupId($attr_group_id);
        $attributeField->save();
        $attr_field_id = $attributeField->getId();

        return $attr_field_id;
    }

    /**
     * Create attribute field Option if it does not exist
     *
     * @param int    $attr_field_id Attr. Field ID
     * @param string $option_label  Attr. Option label
     *
     * @return int attribute Option ID
     *
     */
    protected function addAttributeFieldOption($attr_field_id, $option_label)
    {
        $attributeField = Mage::getModel('catalog/resource_eav_attribute');
        $attributeField->load($attr_field_id);

        $attr_option_id = $this->getAttributeOptionIdByName($attributeField, $option_label);
        if (!$attr_option_id) {
            $data['option'] = array(
                'value' => array('option_0' => array($option_label, $option_label)),
                'order' => array('option_0' => 1),
            );
            $attributeField->addData($data);
            $attributeField->save();

            $attr_option_id = $this->getAttributeOptionIdByName($attributeField, $option_label);
        }

        return $attr_option_id;
    }

    protected function getAttributeGroupIdByName($attr_group_name, $attr_set_id)
    {
        $attr_group_id = null;
        $groups = Mage::getModel('eav/entity_attribute_group')
            ->getResourceCollection()
            ->setAttributeSetFilter($attr_set_id)
            ->load();

        foreach ($groups as $group) {
            if ($group->getAttributeGroupName() == $attr_group_name) {
                $attr_group_id = $group->getAttributeGroupId();
                break;
            }
        }

        return $attr_group_id;
    }

    protected function getAttributeOptionIdByName($attributeField, $option_label)
    {
        $attr_option_id = null;
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute_options_model->setAttribute($attributeField);
        $options = $attribute_options_model->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $option_label) {
                $attr_option_id = $option['value'];
                break;
            }
        }

        return $attr_option_id;
    }

    protected function getAttributeFieldCode($attr_field_id)
    {
        $attributeField = $attributeField = Mage::getModel('catalog/resource_eav_attribute');
        $attributeField->load($attr_field_id);

        return $attributeField->getAttributeCode();
    }

}