<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 26.02.16
 * Time: 15:52
 */

class Web4pro_Category2attributeset_Model_Resource_Filter2_Store_Collection extends
    Mana_Filters_Resource_Filter2_Store_Collection {

    protected function _initSelect()
    {
        $this->getSelect()->from(array('main_table' => $this->getMainTable()));

        $globalEntityName = Mage::helper('mana_db')->getGlobalEntityName($this->getEntityName());
        Mage::helper('mana_db')->joinLeft($this->getSelect(),
            'global', Mage::getSingleton('core/resource')->getTableName($globalEntityName),
            'main_table.global_id = global.id');

        $this->getSelect()
            ->joinLeft(array('ea' => $this->getTable('eav/attribute')), "`ea`.`attribute_code` = `global`.`code` AND `ea`.`attribute_code` <> 'category'", null)
            ->joinLeft(array('et' => $this->getTable('eav/entity_type')),
                "`et`.`entity_type_id` = `ea`.`entity_type_id` AND `et`.`entity_type_code` = 'catalog_product'", null)
            ->joinLeft(array('ca' => $this->getTable('catalog/eav_attribute')), "`ca`.`attribute_id` = `ea`.`attribute_id`", null)
            ->where("`global`.`type` = 'category' OR global.type='stock' OR (`et`.`entity_type_id` IS NOT NULL AND `ca`.`is_filterable` <> 0)");
        return $this;
    }
} 