<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 26.02.16
 * Time: 11:55
 */

class Web4pro_Category2attributeset_Model_Filter_Stock extends Mage_Catalog_Model_Layer_Filter_Abstract
    implements Mana_Filters_Interface_Filter {

    const FILTER_NAME = 'Stock Status';
    const STOCK_IN_STOCK = 1;
    const STOCK_OUT_STOCK = 2;

    protected function _construct(){
        $this->_requestVar = 'stock';
    }


    public function init(){

    }

    /**
     * Returns whether this filter is applied
     *
     * @return bool
     */
    public function isApplied(){
        $appliedValues = $this->getMSelectedValues();

        return !empty($appliedValues);
    }

    /**
     * Applies filter values provided in URL to a given product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return void
     */
    public function applyToCollection($collection){
        $filter = Mage::app()->getRequest()->getParam($this->getRequestVar());

        $cs = $collection->getTable('cataloginventory/stock_status');
        switch($filter){
            case self::STOCK_IN_STOCK:
                $collection->joinTable(array('cs'=>$cs),'product_id=entity_id',array('stock_status'),'cs.stock_status=1');
                break;
            case self::STOCK_OUT_STOCK:
                $collection->joinTable(array('cs'=>$cs),'product_id=entity_id',array('stock_status'),'cs.stock_status=0');
                break;
        }
    }

    /**
     * Returns true if counting should be done on main collection query and false if a separated query should be done
     * Typically it should return false; however there are some cases (like not applied Solr facets) when it should
     * return true.
     *
     * @return bool
     */
    public function isCountedOnMainCollection(){
        return false;
    }

    /**
     * Applies counting query to the current collection. The result should be suitable to processCounts() method.
     * Typically, this method should return final result - option id/count pairs for option lists or
     * min/max pair for slider. However, in some cases (like not applied Solr facets) this method returns collection
     * object and later processCounts() extracts actual counts from this collections.
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return mixed
     */
    public function countOnCollection($collection){
        return $this->_getResource()->countOnCollection($collection);
    }

    public function getRangeOnCollection($collection){
        return array();
    }

    /**
     * Returns option id/count pairs for option lists or min/max pair for slider. Typically, this method just returns
     * $counts. However, in some cases (like not applied Solr facets) this method gets a collection object with Solr
     * results and extracts those results.
     *
     * @param mixed $counts
     * @return array
     */
    public function processCounts($counts){
        return $counts;
    }

    /**
     * Returns whether a given filter $modelToBeApplied should be applied when this filter is being counted. Typically,
     * returns true for all filters except this one.
     *
     * @param $modelToBeApplied
     * @return mixed
     */
    public function isFilterAppliedWhenCounting($modelToBeApplied){
        return false;
    }

    /**
     * Adds all selected items of this filters to the layered navigation state object
     *
     * @return void
     */
    public function addToState(){
        foreach ($this->getMSelectedValues() as $optionId) {
            $label = $this->getLabel($optionId);
            $this->getLayer()->getState()->addFilter(
                $this->_createItemEx(
                    array(
                        'label' => $label,
                        'value' => $optionId,
                        'm_selected' => true,
                        'm_show_selected' =>false,
                    )
                )
            );
        }
    }

    protected function _getResource(){
        return Mage::getResourceModel('web4procategory2attributeset/filter_stock');
    }


    public function getMSelectedValues()
    {
        /* @var $core Mana_Core_Helper_Data */
        $core = Mage::helper(strtolower('Mana_Core'));

        $values = $core->sanitizeRequestNumberParam(
            $this->_requestVar,
            array(array('sep' => '_', 'as_string' => true))
        );

        return $values ? array_filter(explode('_', $values)) : array();
    }

    /**
     * Get filter name
     *
     * @return string
     */
    public function getName()
    {
//        $name = Mage::getConfig()->getNode(self::CONFIG_PREFIX.self::STATE_LABEL);
        return Mage::helper('web4procategory2attributeset')->__(self::FILTER_NAME);
    }

    /**
     * Get data array for building status filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        if ($this->_activeFilter) {
            return array();
        }
        $key = $this->getLayer()->getStateKey().'_STOCK';
        $data = $this->getLayer()->getAggregator()->getCacheData($key);

        $query = $this->getQuery();
        $optionsCount = $query->getFilterCounts('stock');
        if ($data === null) {
            $data = array();
            foreach ($this->getStatuses() as $status) {
                if($status==2){
                    $optionsCount[$status] = $optionsCount[0];
                }
                $data[] = array(
                    'label' => $this->getLabel($status),
                    'value' => $status,
                    'count' => (int)$optionsCount[$status]
                );
            }

            $tags = $this->getLayer()->getStateTags();
            $this->getLayer()->getAggregator()->saveCacheData($data, $key, $tags);
        }
        return $data;
    }

    /**
     * get available statuses
     * @return array
     */
    public function getStatuses() {
        return array(
            self::STOCK_IN_STOCK,
            self::STOCK_OUT_STOCK
        );
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return array(
            self::STOCK_IN_STOCK => Mage::helper('web4procategory2attributeset')->__('In Stock'),
            self::STOCK_OUT_STOCK => Mage::helper('web4procategory2attributeset')->__('Out of Stock')
        );
    }

    /**
     * @param $value
     * @return string
     */
    public function getLabel($value)
    {
        $labels = $this->getLabels();
        if (isset($labels[$value])) {
            return $labels[$value];
        }
        return '';
    }

    public function itemHelper() {
        return Mage::helper('mana_filters/item');
    }

    protected function _createItemEx($data)
    {
        return Mage::getModel('mana_filters/item')
            ->setData($data)
            ->setFilter($this);
    }
} 