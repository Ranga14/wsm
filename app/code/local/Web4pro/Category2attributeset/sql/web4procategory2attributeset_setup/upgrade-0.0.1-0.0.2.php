<?php

$installer = Mage::getResourceModel('catalog/setup', 'default_setup');
$installer->startSetup();

$installer->addAttribute(
    'catalog_product',
    'wsm_brand', // aurorabrand
    array(
        'input' => 'select',
        'type' => 'int',
        'label' => 'WSM Brand',
        'group' => 'General', // this will add to all attribute sets in the General group
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'source' => "web4procategory2attributeset/attribute_source_aurorabrands",
        'required' => false,
        'user_defined' => false,
        'is_configurable' => false,
    )
);

$installer->endSetup();