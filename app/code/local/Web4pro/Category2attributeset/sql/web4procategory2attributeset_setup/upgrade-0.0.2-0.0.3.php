<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 26.02.16
 * Time: 15:05
 */
$installer = $this;

$installer->startSetup();

$data = array(
    'code'=>'stock',
    'display'=>'standard',
    'type'=>'stock',
    'name'=>'Stock status',
    'is_enabled'=>1,
    'is_enabled_in_search'=>1
);

$filter = Mage::getModel('mana_filters/filter2')->addData($data)->save();

$global_id = $filter->getId();

foreach(Mage::app()->getStores() as $store){
    $data['global_id'] = $global_id;
    $data['store_id'] = $store->getId();
    Mage::getModel('mana_filters/filter2_store')->setData($data)->save();
}


$installer->endSetup();

