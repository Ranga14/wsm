<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 24.10.16
 * Time: 12:11
 */

class Web4pro_Category2attributeset_Block_Catalogsearch_Result extends Mage_CatalogSearch_Block_Result {

    public function setListOrders()
    {
        $category = Mage::getSingleton('catalog/layer')->getCurrentCategory();
        /* @var $category Mage_Catalog_Model_Category */
        $availableOrders = $category->getAvailableSortByOptions();
        unset($availableOrders['position']);
        $availableOrders = array_merge(array(
            'relevance' => $this->__('Relevance')
        ), $availableOrders);

        $sortOrder = 'relevance';
        if(isset($availableOrders['price'])){
            $sortOrder = 'price';
        }

        $this->getListBlock()
            ->setAvailableOrders($availableOrders)
            ->setDefaultDirection('desc')
            ->setSortBy($sortOrder);

        return $this;
    }
} 