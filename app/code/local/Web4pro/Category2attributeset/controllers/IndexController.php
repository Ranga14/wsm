<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 22.06.17
 * Time: 18:52
 */

class Web4pro_Category2attributeset_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction(){
        $result = array();
        $layer = Mage::getSingleton('catalog/layer');
        $firearmsCategory = array(19,97,105);
        $categories = Mage::getModel('catalog/category')->getCollection()
                                                      ->addAttributeToFilter('entity_id',array('in'=>$firearmsCategory));
        foreach($categories->getItems() as $category){
            $collection = $layer->setCurrentCategory($category)->getProductCollection();
            $collection->addAttributeToSelect('wsm_capacity')->addAttributeToSelect('caliber')
                       ->addAttributeToSelect('wsm_caliber');
            $layer->prepareProductCollection($collection);
            foreach($collection->getItems() as $product){
                $numrounds = preg_replace('(\(.+\))','',$product->getAttributeText('wsm_capacity'));
                $numrounds = preg_replace('^[0-9]\-^','',$numrounds);
                $numrounds = preg_replace('~\&.+~','',$numrounds);
                $numrounds = preg_replace('~And.+~','',$numrounds);
                $numrounds = preg_replace('~/.+~','',$numrounds);
                $numrounds = preg_replace('~([^0-9]^\.^\+)~','',$numrounds);
                if(strstr($numrounds,'+')){
                    $nums = explode('+',$numrounds);
                    $numrounds = array_sum($nums);
                }
                $result[] = array('type'=>'firearm',
                    'description'=>$product->getName(),
                    'url'=>$product->getProductUrl(),
                    'price'=>$product->getPrice(),
                    'numrounds'=>(float)$numrounds,
                    'mpn'=>$product->getSku(),
                    'caliber'=>$product->getAttributeText('wsm_caliber')?$product->getAttributeText('wsm_caliber'):(string)$product->getAttributeText('caliber'));
            }
        }
        echo json_encode($result);
    }
} 