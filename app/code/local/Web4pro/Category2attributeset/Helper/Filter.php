<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 26.02.16
 * Time: 15:59
 */

class Web4pro_Category2attributeset_Helper_Filter extends Mana_Filters_Helper_Data {

    protected function _getAttributeCodes($setIds) {
        return array_merge(parent::_getAttributeCodes($setIds),array('stock'));
    }

} 