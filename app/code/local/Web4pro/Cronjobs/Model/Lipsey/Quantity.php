<?php

class Web4pro_Cronjobs_Model_Lipsey_Quantity extends Web4pro_Cronjobs_Model_ImportAbstract
{

    const LIPSEY_QUANTITY_IMPORTER_LOG = 'lipsey_quantity.log';

    public function __construct()
    {
        parent::__construct();
        $this->settingsTable = 'aurora_lipseys'; //Settings for this feed
        $this->itemsTable = 'aurora_items_lipseys'; //keeping track of our items
        $this->getFeedSettings();
        $this->wsmAttributes->setAttributeSetName('WSM Lipseys');
        $this->brandSource = 'Lipseys';
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);

        $this->processQuantityFeed();

        Mage::log('Lipsey Quantity index execution time in seconds: ' . (microtime(true) - $time_start), null, self::LIPSEY_QUANTITY_IMPORTER_LOG, true);
        echo 'Lipsey Quantity index execution time in seconds: ' . (microtime(true) - $time_start);
    }



    public function processQuantityFeed()
    {
        echo 'startDownload
        ';
        $feedFile = $this->downloadQuantityFeed();
        $xml = simplexml_load_file($feedFile);

        //Loop over parsed xml
        foreach($xml->children() as $data)
        {
            try {
                $query = "
                UPDATE aurora_items_lipseys
                SET
                price = :price,                                
                quantity =  :quantity,
                mapp = :mapp
                WHERE
                upc = :upc
                ";

                $bindArray = array(
                    'price' => $data->Price->__toString(),
                    'quantity' => $data->QtyOnHand->__toString(),
                    'mapp' => $data->RetailMAP->__toString(),
                    'upc' => $data->UPC->__toString()
                );
                $this->writeConnection->query($query, $bindArray);
            }
            catch(Exception $e)
            {
                Mage::log($e->getMessage(), null, self::LIPSEY_QUANTITY_IMPORTER_LOG, true);
                Mage::logException($e);
            }
        }

        //Update any items that might have a price of 0 in case the feed resolver could miss them later
        $query='
        
          UPDATE catalog_product_entity_decimal cped
          left join aurora_items_lipseys ai
          on  cped.entity_id = ai.magento_id
          left join eav_attribute ea
          on cped.attribute_id = ea.attribute_id
          left join aurora_brands ab
          on ai.aurora_brand_id = ab.id_primary
          SET cped.value = (ai.price * 1.15)
          where ai.id is not null
          and ea.attribute_code = \'price\'          
          and cped.value < 0.1;

        ';

        $this->writeConnection->query($query);
    }

    private function downloadQuantityFeed()
    {
        $url = $this->quantityUrl . '?email=' . $this->userName . '&pass=' . $this->password;
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'QuantityXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . 'lipseysQuantityAndPrice.xml';
        if(!file_exists($fullFilePath))
        {
            Mage::log('Begin download file ' . $fullFilePath, null, self::LIPSEY_QUANTITY_IMPORTER_LOG, true);
            file_put_contents($fullFilePath, fopen($url, 'r'));
            Mage::log('Finished download file' . $fullFilePath, null, self::LIPSEY_QUANTITY_IMPORTER_LOG, true);
        }

        return $fullFilePath;
    }


}