<?php

class Web4pro_Cronjobs_Model_Rsr extends Web4pro_Cronjobs_Model_ImportAbstract
{

    const RSR_INVENTORY_LOG = 'Rsr.log';
    //Configurable settings
    protected $ftp;
    protected $userId;
    protected $password;
    protected $ftpDirectory;
    protected $fileName;
    protected $resource;
    protected $writeConnection;
    protected $readConnection;
    protected $tempDir;
    protected $lastAttributeRow;

    protected $wsmAttributes;

    public function __construct()
    {
        parent::__construct();
        $this->settingsTable = 'aurora_rsr'; //Settings for this feed
        $this->itemsTable = 'aurora_items_rsr';
        $this->wsmAttributes->setAttributeSetName('WSM RSR');
        $this->getFeedSettings();
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);
        $this->processFeed();
        Mage::log('Rsr Inventory execution time in seconds: ' . (microtime(true) - $time_start), null, self::RSR_INVENTORY_LOG, true);
    }


    private function processFeed()
    {
        $file = $this->downloadFeed();
        $handle = fopen($file, 'r');

        //Get wsm_brand attribute id. Needed for proper sorting
        $query = "
        select attribute_id from eav_attribute e
        where e.attribute_code = 'wsm_brand'
        ";

        $wsmBrandAttributeId = $this->readConnection->fetchOne($query);

        $counterSql = 'SELECT MAX(brand_id) FROM aurora_brands';
        $brandCounter = (int) $this->readConnection->fetchOne($counterSql);

        while (($row = fgetcsv($handle, 1000, ";")) !== FALSE)
        {

            $type = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
            $product_attribute_set_id = 4;
            $rsrNumber = $row[0];
            $sku = $row[1];
            $itemName = $row[2];
            $departmentId = (int) $row[3];
            $manufacturerId = $row[4];
            $msrp = $row[5];
            $price = $row[6];
            $weight = $row[7];
            $quantity = $row[8];
            $model = $row[9];
            $brand = $row[10];
            $description = $row[13];
            $mapp = 0; //$row[70];

            //Test for UPC. If no UPC use item number as sku
            if(!$row[1])
            {
                $sku = 'rsr' . $rsrNumber;
            }
            elseif(substr($sku, 0, 1) == '0')
            {
                //Get rid of leading zeros to match up with sports south.
                $sku = ltrim($sku, '0');
            }

            //Does Brand already exist? If not insert it
            if(!isset($this->brands[$brand]))
            {
                $query='
                INSERT INTO aurora_brands(brand_name, brand_id, brand_url, brand_item_count, source)
                VALUES (:brandName, :brandNo, :brandUrl, :brandItemCount, :source)
                ';

                $bindArr = array(
                    'brandName'      => $brand,
                    'brandNo'        => ++$brandCounter,
                    'brandUrl'       => '',
                    'brandItemCount' => 1,
                    'source'         => 'RSR'
                );
                $this->writeConnection->query($query, $bindArr);

                $this->brands[$brand] = array(
                    'brand_name'       => $brand,
                    'id_primary'         => $this->writeConnection->lastInsertId(),
                    'brand_url'        => '',
                    'brand_item_count' => 1,
                    'source'           => 'RSR',
                    'use_mapp'         => 0,
                    'brand_id'         => $brandCounter
                );
            }


            $productCategoryArray = $this->resolveCategory($departmentId);
            $isInStock = $quantity > 0 ? 1 : 0;

            $productData = array(
                'categories' => $productCategoryArray,
                'name' => $itemName,
                'description' => $itemName,
                'short_description' => $description,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => $weight,
                'price' => (( ((int) $this->brands[$brand]['use_mapp']) == 1) && $mapp ? $mapp : $price * 1.15),
                'qty' => $quantity,
                'is_in_stock' => $isInStock,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => $msrp,
                'meta_title' => $itemName,
                'meta_description' => $itemName,
                'meta_keyword' => $itemName . ' ' . $brand . ' rsr-' . $rsrNumber,
                'wsm_brand'=> $this->brands[$brand]['brand_id']
            );

            try
            {

                $query = "select entity_id
                from catalog_product_entity
                where sku = :sku";

                $findProduct = $this->readConnection->fetchOne($query, array('sku'=>$sku));

                if(!$findProduct)
                {
                    $newProduct = Mage::getModel('catalog/product_api')->create($type, $product_attribute_set_id, $sku, $productData);
                    $query = '
                                INSERT into aurora_items_rsr
                                (magento_id, upc, rsr_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :rsrId, :price, :msrp, :mapp, :quantity, :brandId )
                                ';

                    $bindArray = array(
                        'magentoId'=> $newProduct,
                        'sku' => $sku,
                        'rsrId' => $rsrNumber ,
                        'price' => $price,
                        'msrp' => $msrp,
                        'mapp' => $mapp,
                        'quantity' => $quantity,
                        'brandId' => $this->brands[$brand]['id_primary']
                    );

                    $this->writeConnection->query($query, $bindArray);
                    $fullFilePath = $this->getImage($rsrNumber);

                    if($fullFilePath)
                    {
                        $product = Mage::getModel('catalog/product')->loadByAttribute('entity_id', $newProduct, 'entity_id');
                        $product->addImageToMediaGallery($fullFilePath, array('small_image','thumbnail', 'image'), true);
                        $product->save();
                    }
                }
                else
                {
                    //Add Brand attribute
                    $query = '
                        REPLACE INTO catalog_product_entity_int
                        (entity_type_id, attribute_id, store_id, entity_id, `value`)
                        VALUES( 4, :wsmBrandAttributeId, 0, :magentoId, :brandNo);
                          ';

                    $bindArray = array (
                        'wsmBrandAttributeId' => $wsmBrandAttributeId,
                        'magentoId' => $findProduct,
                        'brandNo'=>$this->brands[$brand]['brand_id']
                    );


                    $this->writeConnection->query($query, $bindArray);

                    $query = "
                    SELECT 1 FROM aurora_items_rsr
                    WHERE upc = :sku";

                    $result = $this->readConnection->fetchOne($query,array('sku'=>$sku));

                    if($result)
                    {
                        $query = "
                                UPDATE aurora_items_rsr
                                SET
                                price = :price,
                                msrp = :msrp,
								mapp= :mapp,
                                quantity =  :quantity,
                                aurora_brand_id = :brandId,
                                rsr_id = :rsrId
                                WHERE
                                upc = :sku
                                ";
                        $bindArray = array(
                            'price' => $price,
                            'msrp' => $msrp,
                            'mapp' => $mapp,
                            'quantity' => $quantity,
                            'brandId' => $this->brands[$brand]['id_primary'],
                            'rsrId' => $rsrNumber,
                            'sku' => $sku
                        );
                        $this->writeConnection->query($query, $bindArray);
                    }
                    else
                    {
                        $query = '
                                INSERT into aurora_items_rsr
                                (magento_id, upc, rsr_id, price, msrp, mapp, quantity, aurora_brand_id)
                                VALUES
                                (:magentoId, :sku, :rsrId, :price, :msrp, :mapp, :quantity, :brandId )
                                ';
                        $bindArray = array(
                            'magentoId'=> $findProduct,
                            'sku' => $sku,
                            'rsrId' => $rsrNumber ,
                            'price' => $price,
                            'msrp' => $msrp,
                            'mapp' => $mapp,
                            'quantity' => $quantity,
                            'brandId' => $this->brands[$brand]['id_primary']
                        );
                        $this->writeConnection->query($query, $bindArray);
                    }

                }
            }
            catch (Exception $e)
            {
                Mage::log($e->getMessage(), null, self::RSR_INVENTORY_LOG, true);
                Mage::logException($e);
            }

        }
    }

    private function downloadFeed()
    {
        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'brandXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $this->fileName;
        if(!file_exists($fullFilePath))
        {
            echo "download file <br/>\n";
            //File isn't here download it
            $connection = ftp_connect($this->ftp) or die("Could not connect to {$this->ftp}");;
            $loginAttempt = ftp_login($connection, $this->userId, $this->password);
            if(!$loginAttempt)
            {
                //Couldn't Login
                die();
            }
            $saved = ftp_get($connection, $fullFilePath, $this->ftpDirectory . $this->fileName, FTP_ASCII);
            if(!$saved)
            {
                echo ('couldn\'t download ' . $this->fileName);
                //Error downloading file
                unlink($fullFilePath);
                die();
            }
            ftp_close($connection);
        }

        return $fullFilePath;
    }

    private function getImage($rsrNumber)
    {
        $firstChar = substr($rsrNumber,0,1);
        if(is_numeric($firstChar))
        {
            $firstChar = '#';
        }


        //Download image file to tmp directory
        $folderPath =  $this->tempDir . 'rsrImages/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . $rsrNumber . '.jpg';


        $connection = ftp_connect($this->ftp);
        if($connection === false)
        {
            return false;
        }
        $loginAttempt = ftp_login($connection, $this->userId, $this->password);
        if(!$loginAttempt)
        {
            //Couldn't Login
            return false;
        }

        $saved = ftp_get($connection, $fullFilePath, $this->ftpImageDirectory . strtolower($firstChar) . '/' . $rsrNumber . '_1.jpg' , FTP_BINARY);
        if(!$saved)
        {
            //Error downloading file
            unlink($fullFilePath);
            return false;
        }
        Mage::log('Download image: ' . $fullFilePath, null, self::RSR_INVENTORY_LOG, true);
        ftp_close($connection);

        return $fullFilePath;
    }

    protected function resolveCategory($departmentId)
    {
        $productCategoryArray = array();
        $rsrCategoryMapArray = array(
            0 => 'Other',
            1 => 'Firearms',
            2 => 'Used Guns',
            3 => 'Used Guns',
            4 => 'Other',
            5 => 'Firearms',
            6 => 'NFA | Class 3',
            7 => 'Black Powder Firearms',
            8 => 'Optics',
            9 => 'Optics',
            10 => 'Firearm Accessories',
            11 => 'Firearm Accessories',
            12 => 'Firearm Accessories',
            13 => 'Other',
            14 => 'Firearm Accessories',
            15 => 'Reloading Components',
            16 => 'Black Powder Firearms',
            17 => 'Other',
            18 => 'Ammunition',
            19 => 'Other',
            20 => 'Other',
            21 => 'Other',
            22 => 'Other',
            23 => 'Other',
            24 => 'Firearm Accessories',
            25 => 'Other',
            26 => 'Other',
            27 => 'Other',
            28 => 'Optics',
            29 => 'Optics',
            30 => 'Optics',
            31 => 'Optics',
            32 => 'Firearm Components',
            33 => 'Other',
            34 => 'Firearm Components',
            35 => 'Firearm Accessories',
            36 => 'Other',
            37 => 'Other',
            38 => 'Other',
            39 => 'Other',
            40 => 'Firearm Accessories',
            41 => 'Firearm Components',
            42 => 'Firearm Components',
            43 => 'Firearm Components',
        );

        $newCatString = $rsrCategoryMapArray[$departmentId];




        $productCategoryArray['0'] = (int)( $this->categories[$newCatString]? : 104 );

        $firearmArray = array(
            'Tactical rifles',
            'Rifles',
            'Pistols',
            'Revolvers',
            'Tactical shotguns',
            'Shotguns',
            'Combo',
            'Specialty',
            'Air guns'
        );

        $accessoriesArray = array(
            'Magazines and accessories',
            'Stocks and recoil pads',
            'Holsters',
            'Slings',
            'Cleaning kits',
            'Batteries',
            'Holders and accessories',
            'Air gun accessories',
            'Carrying bags',
            'Guncases',
            'Gun vaults and safes',
            'Tools',
            'Targets',
            'Cleaning and restoration',
            'Gun rests - bipods - tripods',
            'Media'
        );

        $componentsArray = array(
            'Conversion kits',
            'Uppers',
            'Lowers',
            'Firearm parts',
            'Choke tubes',
            'Swivels',
            'Rings and adapters',
            'Extra barrels'
        );

        $opticsArray = array(
            'Red dot scopes',
            'Scopes',
            'Night vision',
            'Gun sights',
            'Laser sights',
            'Bases',
            'Scope covers and shades',
            'Bore sighters and arbors',
            'Rage finders',
            'Binoculars',
            'Cameras'
        );

        $ammunitionArray = array(
            'Centerfire handgun rounds',
            'Centerfire rifle rounds',
            'Rimfire rounds',
            'Shotshell buckshot loads',
            'Shotshell slug loads',
            'Shotshell lead loads',
            'Shotshell non-tox loads',
            'Shotshell steel loads',
            'Blank rounds',
            'Black power bullets',
            'Air gun ammo',
            'Dummy rounds'
        );

        $nfa = array(
            'Suppressors'
        );

        $reloadingArray = array(
            'Components',
            'Powders',
            'Reloading bullets',
            'Presses',
            'Dies',
            'Reloading accessories',
            'Utility boxes',
            'Black power accessories'
        );

        $huntingArray = array(
            'Apparel',
            'Eye protection',
            'Hearing protection',
            'Personal protection',
            'Electronics',
            'Accessories miscellaneous',
            'Blinds and accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knife accessories',
            'Coolers',
            'Repellents',
            'Displayes',
            'Camping',
            'Knive accessories',
            'Hunting scents',
            'Lights',
            'Spotting',
            'Knives',
            'Feeders',
            'Game calls',
            'Archery and accessories',
            'Traps and clay throwers',
            'Decoys',
            'Atv accessories'
        );

        $blackPowderArray = array(
            'Frames'
        );

        if(in_array($newCatString, $firearmArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 97;
        }
        elseif(in_array($newCatString, $opticsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 98;
        }
        elseif(in_array($newCatString, $ammunitionArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 6;
        }
        elseif(in_array($newCatString, $accessoriesArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 100;
        }
        elseif(in_array($newCatString, $componentsArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 101;
        }
        elseif(in_array($newCatString, $nfa))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 103;
        }
        elseif(in_array($newCatString, $reloadingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 99;
        }
        elseif(in_array($newCatString, $huntingArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 102;
        }
        elseif(in_array($newCatString, $blackPowderArray))
        {
            $productCategoryArray['1'] = $productCategoryArray['0'];
            $productCategoryArray['0'] = 19;
        }

        return $productCategoryArray;
    }

}

