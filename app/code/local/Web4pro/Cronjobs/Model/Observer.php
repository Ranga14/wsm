<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 17.03.18
 * Time: 9:59
 */

class Web4pro_Cronjobs_Model_Observer extends Mage_Core_Model_Observer {

    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        parent::cleanCache($schedule);
    }
}