<?php

class Web4pro_Cronjobs_Model_Lipsey extends Web4pro_Cronjobs_Model_ImportAbstract
{

    const LIPSEY_IMPORTER_LOG = 'lipsey.log';

    public function __construct()
    {
        parent::__construct();
        $this->settingsTable = 'aurora_lipseys'; //Settings for this feed
        $this->itemsTable = 'aurora_items_lipseys'; //keeping track of our items
        $this->getFeedSettings();
        $this->wsmAttributes->setAttributeSetName('WSM Lipseys');
        $this->brandSource = 'Lipseys';
    }

    public function run()
    {
        set_time_limit(0);
        $time_start = microtime(true);

        $this->processInventoryFeed();

        Mage::log('Lipsey Inventory execution time in seconds: ' . (microtime(true) - $time_start), null, self::LIPSEY_IMPORTER_LOG, true);
        echo 'Lipsey Inventory execution time in seconds: ' . (microtime(true) - $time_start);
    }


    public function processInventoryFeed()
    {
        echo 'startDownload
        ';
        $feedFile = $this->downloadInventoryFeed();
        $xml = simplexml_load_file($feedFile);

        //Loop over parsed xml
        foreach($xml->children() as $data)
        {
            //Pull data from xml entry
            $itemNo =  $data->ItemNo->__toString();
            $desc1 =  $data->Desc1->__toString();
            $desc2 =  $data->Desc2->__toString();
            $upc = $data->UPC->__toString();
            $mfg = trim($data->MFG->__toString());
            $model =  $data->Model->__toString();
            $image =  $data->Image->__toString();
            $itemType =  $data->ItemType->__toString();

            //Prepare some data
            $categoryArray = $this->resolveCategory($itemType);
            $this->checkBrandExists($mfg);
            $upc = ($upc!='')? $upc : 'lip'. $itemNo;

            //Magento product to insert
            $productData = array(
                'categories' => $categoryArray,
                'name' => $model . ' ' . $itemNo,
                'description' => $desc1 . ' <br/>' . $desc2,
                'short_description' => $desc1 . ' <br/>' . $desc2,
                'website_ids' => array('base'), // Id or code of website
                'status' => 1, // 1 = Enabled, 2 = Disabled
                'visibility' => 4, // 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
                'weight' => (string) $data->Weight,
                'price' => 0,
                'qty' => 0.00,
                'is_in_stock' => 0,
                'manage_stock' => 1,
                'use_config_manage_stock' => 0,
                'tax_class_id' => 2,
                'msrp' => (float) $data->MSRP,
                'meta_title' => $itemNo,
                'meta_description' => $itemNo,
                'meta_keyword' => $mfg . ' lipseys-' . $itemNo,
                'wsm_brand'=> $this->brands[$mfg]['brand_id']
            );

            try
            {
                //Insert product into Magento and aurora tables.
                $insertProduct = $this->insertProduct($productData, $upc, $itemNo, $mfg);
                $product = Mage::getModel('catalog/product')->load($insertProduct['magentoId']);
                $helper = Mage::helper('web4procronjobs/catalog');
                if (!$helper->checkProductImageExist($product)) {
                    $file = $this->downloadImage($image);
                    $product->addImageToMediaGallery($file, array('small_image', 'thumbnail', 'image'), true);
                    $product->save();
                }

                //Insert Attributes
                $entityId = $insertProduct['magentoId'];
                $this->wsmAttributes->setAttributeField($entityId, 'safety', $data->Safety->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'length', $data->Length->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'receiver', $data->Receiver->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'sights', $data->Sights->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'stock frame grips', $data->StockFrameGrips->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'magazine', $data->Magazine->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'chamber', $data->Chamber->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'drilled tapped', $data->DrillTapped->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'rate of twist', $data->RateOfTwist->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'feature1', $data->Feature1->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'feature2', $data->Feature2->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'feature3', $data->Feature3->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'shipping weight', $data->ShippingWeight->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'bound book model', $data->BoundBookModel->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'bound book type', $data->BoundBookType->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'finish color', $data->Finish->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'bound book mfg', $data->BoundBookMFG->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa thread pattern', $data->NFAThreadPattern->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa attach method', $data->NFAAttachMethod->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'caliber', $data->Caliber->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa baffle', $data->NFABaffle->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa can disassemble', $data->NFACanDisassemble->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa construction', $data->NFAConstruction->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa db reduction', $data->NFAdbReduction->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa diameter', $data->NFADiameter->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'nfa form 3 caliber', $data->NFAForm3Caliber->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'magnification', $data->Magnification->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'maintube', $data->Maintube->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'objective', $data->Objective->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'adjustable objective', $data->AdjustableObjective->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'optic adjustments', $data->OpticAdjustments->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'reticle', $data->Reticle->__toString());
                $this->wsmAttributes->setAttributeField($entityId, 'illuminated reticle', $data->IlluminatedReticle->__toString());

            }

            catch (Exception $e)
            {
                Mage::log($e->getMessage(), null, self::LIPSEY_IMPORTER_LOG, true);
                Mage::logException($e);
            }
        }
        echo 'Finish Download
        ';
    }

    private function downloadInventoryFeed()
    {
        $url = $this->itemsUrl . '?email=' . $this->userName . '&pass=' . $this->password;

        //Download inventory file to tmp directory
        $folderPath =  $this->tempDir . 'FeedXml/' . date('n/j/Y') . '/';
        if(!is_dir($folderPath))
        {
            mkdir($folderPath,0777, true);
        }

        $fullFilePath = $folderPath . 'lipseysInventory.xml';
        if(!file_exists($fullFilePath))
        {
            echo "begin download file <br/>\n";
            file_put_contents($fullFilePath, fopen($url, 'r'));
            echo "finished download file <br/>\n";
        }

        return $fullFilePath;
    }

    private function downloadImage($image)
    {
        /** @var Web4pro_Cronjobs_Helper_Copyresize $helper */
        $helper = Mage::helper('web4procronjobs/copyresize');
        $imageLink = 'http://www.lipseys.net/images/' . trim($image) ;
        $rawImage = $this->getCurlRequest($imageLink);
        $fullFilePath = $this->tempDir . $image;
        fwrite(fopen($fullFilePath, 'w'), $rawImage);
        $helper->resizeImage($fullFilePath);
        return $fullFilePath;
    }

}