<?php
/**
 * Created by PhpStorm.
 * User: dev01
 * Date: 02.04.18
 * Time: 19:02
 */

class Web4pro_Cronjobs_Helper_Catalog extends Mage_Core_Helper_Abstract
{
    protected $_resource;
    protected $_readConnection;
    protected $_writeConnection;

    /**
     * Web4pro_Cronjobs_Helper_Catalog constructor.
     */
    public function __construct()
    {
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
    }

    /**
     * @return array
     */
    public function getExistingCatagories()
    {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();

        $ids = $tree->getCollection()->getAllIds();

        $returnArray = array();
        if ($ids)
        {
            foreach ($ids as $id)
            {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
                if($cat->getLevel() > 0 && $cat->getIsActive()==1)
                {
                    $returnArray[$cat->getName()] = $cat->getId();
                }
            }
        }

        return $returnArray;
    }

    public function getBrandArray()
    {
        $query = '
          SELECT brand_name, id_primary, brand_id, brand_url, brand_item_count, source, use_mapp
          FROM aurora_brands ';
        $existingBrandResults = $this->_writeConnection->fetchAssoc($query);

        return $existingBrandResults;
    }

    private function checkProductImageExist($product)
    {
        if ($product->getImage() && $product->getImage() != 'no_selection') {
            return true;
        }
        return false;
    }


}