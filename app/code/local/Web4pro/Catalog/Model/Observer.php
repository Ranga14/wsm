<?php
/**
 * Created by PhpStorm.
 * User: algol
 * Date: 16.04.2018
 * Time: 22:52
 */

class Web4pro_Catalog_Model_Observer
{
    public function sortProductOutOfStockDescending(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();

        $collection->getSelect()->joinLeft(
            array('_inventory_table' => $collection->getTable('cataloginventory/stock_item')),
            "_inventory_table.product_id = e.entity_id",
            array('is_in_stock')
        )
            ->order('is_in_stock DESC')
            ->order('created_at DESC');
    }
}