// JQuery
$j(document).ready(function() {
	
	/******** Side Nav JS *******/
	
	var catHdr = $j('.cat-hdr');
    var searchHdr = ' .m-filter-item-list';

    // Find how many Classes there are and set Var
    var num = catHdr.length;

    // Append Unique Class Number to each Div
    $j('.m-filter-item-list').each(function(num) {
        $j(this).addClass('toggle-' + num);
    });

    // Append Unique Class Number to each Div
    catHdr.each(function(num) {
        $j(this).addClass('cat-' + num);

        var subItem = $j('.toggle-' + num + ' li');
        var count = subItem.length;

        $j('.cat-' + num + ' .halflings').click(function(e) {
            e.preventDefault();

            if ( $j('.cat-' + num + ' .halflings').hasClass('halflings-menu-up') ) {
                console.log('Open Div');
                
                $j('.cat-' + num + ' .halflings').removeClass('halflings-menu-up').addClass('halflings-menu-down');
                if ( count > 10 ) {
                    $j('.cat-' + num + ' .searchFilter').show();
                }
            } else {
                console.log('Close Div');
                //$j('.cat-' + num + searchHdr).removeClass('show');
                $j('.cat-' + num + ' .halflings').removeClass('halflings-menu-down').addClass('halflings-menu-up');
                if ( count > 10 ) {
                    $j('.cat-' + num + ' .searchFilter').hide();
                }
            }

            $j('.cat-' + num + searchHdr).toggleClass('show');

            if ( count > 10 ) {
                $j('.cat-' + num + ' .scroll-pane').toggleClass('filterHeight').jScrollPane();
                $j('.cat-' + num + ' .jspContainer').toggleClass('filterHeight');
                if ( $j('.cat-' + num + ' .searchFilter').length === 0 ) {
                    $j('<input type="text" class="searchFilter" placeholder="Search...">').appendTo('.cat-' + num);
                }

                if ( $j('.cat-' + num + ' .jspContainer').hasClass('filterHeight') ) {
                    console.log('add height');
                    $j('.cat-' + num + ' .jspContainer').css('height', '200px');
                } else {
                    console.log('remove height');
                    $j('.cat-' + num + ' .jspContainer').css('height', 0);
                }
            }
        });

        $j('.cat-' + num + ' .block-title span').click(function(e) {
            e.preventDefault();

            if ( $j('.cat-' + num + ' .halflings').hasClass('halflings-menu-up') ) {
                console.log('Open Div');
                
                $j('.cat-' + num + ' .halflings').removeClass('halflings-menu-up').addClass('halflings-menu-down');
                if ( count > 10 ) {
                    $j('.cat-' + num + ' .searchFilter').show();
                }
            } else {
                console.log('Close Div');
                //$j('.cat-' + num + searchHdr).removeClass('show');
                $j('.cat-' + num + ' .halflings').removeClass('halflings-menu-down').addClass('halflings-menu-up');
                if ( count > 10 ) {
                    $j('.cat-' + num + ' .searchFilter').hide();
                }
            }

            $j('.cat-' + num + searchHdr).toggleClass('show');

            if ( count > 10 ) {
                $j('.cat-' + num + ' .scroll-pane').toggleClass('filterHeight').jScrollPane();
                $j('.cat-' + num + ' .jspContainer').toggleClass('filterHeight');
                if ( $j('.cat-' + num + ' .searchFilter').length === 0 ) {
                    $j('<input type="text" class="searchFilter" placeholder="Search...">').appendTo('.cat-' + num);
                }

                if ( $j('.cat-' + num + ' .jspContainer').hasClass('filterHeight') ) {
                    console.log('add height');
                    $j('.cat-' + num + ' .jspContainer').css('height', '200px');
                } else {
                    console.log('remove height');
                    $j('.cat-' + num + ' .jspContainer').css('height', 0);
                }
            }
        });
        
        // $j('.cat-' + num + ' .searchFilter').keyup(function(){
        $j('.cat-' + num).on('keyup', '.searchFilter', function(){
            var filter = $j(this).val().toUpperCase();
            var lis = $j('.cat-' + num + searchHdr + ' a');
            for (var i = 0; i < lis.length; i++) {
                var name = lis[i].innerHTML;
                if (name.toUpperCase().indexOf(filter) == 0) {
                    lis[i].style.display = 'block';
                } else {
                    lis[i].style.display = 'none';
                }
            }
        }).blur(function(){
            $j('.cat-' + num + searchHdr + ' a').each(function(val) {
                $j(this).show();
            });
        });
    });

    catHdr.each(function(num) {

        var sub = $j('.toggle-' + num);
        var subItem = $j('.toggle-' + num + ' li');

        sub.each(function() {
            var count = subItem.length;
            console.log('Count: ' + count);

            if ( count > 10 ) {
                $j(this).addClass('hide');
                //$j('.cat-' + num + ' .jspContainer').addClass('filterHeight');
            } else {
                $j(this).addClass('show');
                $j('.cat-' + num + ' .halflings').removeClass('halflings-menu-up').addClass('halflings-menu-down');
                console.log('Add Show as default.');
            }
        });
    });

    // Mobile Filter JS
    $j('.btnFilterAttr').click(function(){
        $j('.sidebar').animate({width:'toggle'},0);
    });

    // Override No Display class, stupid Magento
    $j('.scroll-pane').removeClass('no-display');
    $j('.cat-links').removeClass('no-display');

    var cat = $j('.breadcrumbs strong').text();

    //$j('.cat-links li').hide();
    //$j('.cat-links a:contains(' + cat + ')').parent().show();
    $j('.cat-links a:contains(' + cat + ')').css('color', '#fff');
});