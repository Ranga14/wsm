module.exports = function(grunt) {
  grunt.initConfig({
    // Minify CSS
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['styles.css'],
          dest: 'css',
          ext: '.min.css'
        }]
      }
    },
    // Minify JS
    uglify: {
      dist: {
        files: {
          'js/category.min.js': ['js/jscroller.js', 'js/catfilter.js']
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['cssmin', 'uglify']);
}